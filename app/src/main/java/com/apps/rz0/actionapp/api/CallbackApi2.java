package com.apps.rz0.actionapp.api;

/**
 * Callback для работы с сервером
 */
public interface CallbackApi2<T> {
    public void onSuccess(T object);

    public void onError(String error);
}
