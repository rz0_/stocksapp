package com.apps.rz0.actionapp.stocks;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Александр on 24.08.2015.
 */
public class CompanyItem {
    private String title;
    @SerializedName("company_id")
    private Integer companyId;
    private String avatar;

    public String getTitle() {
        return title;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public String getAvatar() {
        return avatar;
    }
}
