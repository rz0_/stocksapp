package com.apps.rz0.actionapp.api.model;

/**
 * Created by Александр on 19.08.2015.
 */
public class SignedUser {
    private int id;
    private String name;
    private String suname;
    private String city;
    private String avatar;

    public String getAvatar() {
        return avatar;
    }

    public SignedUser setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }

    public int getId() {
        return id;
    }

    public SignedUser setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public SignedUser setName(String name) {
        this.name = name;
        return this;
    }

    public String getSuname() {
        return suname;
    }

    public SignedUser setSuname(String suname) {
        this.suname = suname;
        return this;
    }

    public String getCity() {
        return city;
    }

    public SignedUser setCity(String city) {
        this.city = city;
        return this;
    }
}
