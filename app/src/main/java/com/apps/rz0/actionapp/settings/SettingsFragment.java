package com.apps.rz0.actionapp.settings;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.StocksApplication;
import com.apps.rz0.actionapp.utils.StaticStrings;


/**
 * Фрагмент для показа настроек приложения
 */
public class SettingsFragment extends Fragment {
    AQuery aq;
    SharedPreferences.Editor editor;
    private int PUSH = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        aq = new AQuery(view);

        final SharedPreferences sharedPreferences = getActivity().getSharedPreferences(StaticStrings.SETTINGS_SHARED_PREF, Context.MODE_PRIVATE);
        boolean isPushOn = sharedPreferences.getBoolean(StaticStrings.IS_PUSH_ON, true);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor = sharedPreferences.edit();
                boolean boolValue = (Boolean) v.getTag(R.id.tag_position);
                int type = (Integer) v.getTag(R.id.tag_name);
                if (type == PUSH) {
                    editor.putBoolean(StaticStrings.IS_PUSH_ON, !boolValue);
                    StocksApplication.serverController.savePushStatus(!boolValue, null);
                }
                ((ImageView) v).setImageResource(!boolValue ? R.drawable.ic_toggle_on : R.drawable.ic_toggle_off);
                v.setTag(R.id.tag_position, !boolValue);
                editor.apply();
            }
        };
        aq.id(R.id.turnOnPushImageView).image(isPushOn ? R.drawable.ic_toggle_on : R.drawable.ic_toggle_off)
                .tag(R.id.tag_position, isPushOn).tag(R.id.tag_name, PUSH).clicked(listener);

        RecyclerView themesRecyclerView = (RecyclerView) view.findViewById(R.id.colors_recycler_view);
        themesRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        themesRecyclerView.setAdapter(new ThemesRecyclerAdapter((ActionBarActivity) getActivity()));

        return view;
    }

}
