package com.apps.rz0.actionapp.api.model.company;

import com.apps.rz0.actionapp.stocks.StockItem;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CompanyItem {
    @SerializedName("partner_id")
    public int idPartner;

    @SerializedName("company_name")
    public String companyName;

    public String description;
    public String avatar;

    @SerializedName("comments_num")
    public String comments;

    @SerializedName("likes_num")
    public String likes;

    @SerializedName("views_num")
    public String views;

    @SerializedName("shops_num")
    public String shops;

    public ArrayList<StockItem> stocks;
}
