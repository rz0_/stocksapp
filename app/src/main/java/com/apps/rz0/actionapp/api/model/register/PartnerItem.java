package com.apps.rz0.actionapp.api.model.register;

import com.google.gson.annotations.SerializedName;

public class PartnerItem {
    @SerializedName("partner_id")
    public int idPartner;
    @SerializedName("company_name")
    public String companyName;
    public String description;
    @SerializedName("city_id")
    public int cityId;
    public String avatar;
    @SerializedName("sale_count")
    public String saleCount;
}
