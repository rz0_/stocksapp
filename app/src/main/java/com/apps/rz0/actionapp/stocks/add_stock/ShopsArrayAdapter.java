package com.apps.rz0.actionapp.stocks.add_stock;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.model.shops.ShopItem;
import com.apps.rz0.actionapp.choose_dialog.CityItem;

import java.util.List;

public class ShopsArrayAdapter extends ArrayAdapter<ShopItem> {
    public ShopsArrayAdapter(Context context, int resource, List<ShopItem> objects) {
        super(context, resource, objects);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).idShop;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CountryViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.getContext())
                    .inflate(R.layout.spinner_item_layout, parent, false);

            viewHolder = new CountryViewHolder();
            viewHolder.countryName = (TextView) convertView.findViewById(R.id.item_name);

            convertView.setTag(viewHolder);
            convertView.setTag(R.id.tag_id, getItem(position).idShop);
        } else {
            viewHolder = (CountryViewHolder) convertView.getTag();
        }

        viewHolder.countryName.setText(getItem(position).address);

        return convertView;
    }

    class CountryViewHolder {
        TextView countryName;
    }
}
