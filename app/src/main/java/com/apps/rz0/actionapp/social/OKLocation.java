package com.apps.rz0.actionapp.social;

/**
 * Created by Александр on 19.08.2015.
 */
public class OKLocation {
    private String city;
    private String country;

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }
}
