package com.apps.rz0.actionapp.registration;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.Api;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ForgetPassDialog extends DialogFragment {
    private AQuery aq;
    boolean flagBack = true;
    private EditText phoneEditText;
    public static String TAG = "forget_pass";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forget_pass_dialog, container, false);
        aq = new AQuery(view);
        aq.id(R.id.progress).gone();
        aq.id(R.id.forget_pass_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aq.id(R.id.progress).visible();
                Api.get.recoverPass(aq.id(R.id.mail_edittext).getText().toString(), new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        aq.id(R.id.progress).gone();
                        dismiss();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        aq.id(R.id.progress).gone();
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });


        return view;
    }
}
