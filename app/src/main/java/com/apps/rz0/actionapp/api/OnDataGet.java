package com.apps.rz0.actionapp.api;

/**
 * Created by Александр on 27.08.2015.
 */
public interface OnDataGet<T> {
    void getData(T success);

}
