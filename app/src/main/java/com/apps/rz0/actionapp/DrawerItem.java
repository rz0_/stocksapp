package com.apps.rz0.actionapp;

/**
 * Класс для получения одного элемента бокового меню
 */
public class DrawerItem {
    String title;
    int idIcon;
    boolean isArrowVisible = true;

    public DrawerItem(String title, int idIcon) {
        this.title = title;
        this.idIcon = idIcon;
    }

    public DrawerItem(String title, int idIcon, boolean isArrowVisible) {
        this.title = title;
        this.idIcon = idIcon;
        this.isArrowVisible = isArrowVisible;
    }
}
