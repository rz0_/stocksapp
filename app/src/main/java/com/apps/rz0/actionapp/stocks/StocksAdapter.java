package com.apps.rz0.actionapp.stocks;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.comments.CommentActivity;
import com.apps.rz0.actionapp.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class StocksAdapter extends RecyclerView.Adapter {

    ArrayList<StockItem> stockItems = new ArrayList<>();
    int widthDisplay;
    LayoutInflater inflater;
    ActionBarActivity activity;

    public StocksAdapter(ActionBarActivity activity, ArrayList<StockItem> stockItems) {
        this.stockItems = stockItems;
        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        widthDisplay = display.getWidth();
        inflater = LayoutInflater.from(activity);
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_stock, parent, false);
        StockViewHolder holder = new StockViewHolder(activity, view, widthDisplay);
        holder.itemView.setOnClickListener(new DoubleClickListener() {
            @Override
            public void onSingleClick(View v) {

            }

            @Override
            public void onDoubleClick(View v) {
                likeClick(v);
            }
        });

        holder.likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likeClick(v);
            }
        });
        return holder;
    }

    private void likeClick(View v) {
        StockItem item = (StockItem) v.getTag(R.id.tag_id);
        int position = (Integer) v.getTag(R.id.tag_position);
        Api.get.likeAction(Utils.getRegPushId(v.getContext()), item.getStockId());
        StockItem arrayItem = stockItems.get(position);
        arrayItem.setIsLiked(!item.isLiked());
        if (arrayItem.isLiked()) {
            arrayItem.setLikes(arrayItem.getLikes() + 1);
            showFavoritesToast(v.getContext());
        } else
            arrayItem.setLikes(arrayItem.getLikes() - 1);
        notifyItemChanged(position);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        StockViewHolder holder = (StockViewHolder) viewHolder;
        StockItem item = stockItems.get(position);
        holder.setup(item, position);
    }

    private void showFavoritesToast(Context context) {
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(inflater.inflate(R.layout.add_toast_layout, null, false));
        toast.show();
    }

    @Override
    public int getItemCount() {
        return stockItems.size();
    }

}
