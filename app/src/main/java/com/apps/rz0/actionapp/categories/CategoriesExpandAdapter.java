package com.apps.rz0.actionapp.categories;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.model.categories.CategoryItem;

import java.util.ArrayList;

/**
 * Адаптер для вывода списка категорий
 */
public class CategoriesExpandAdapter extends BaseExpandableListAdapter {

    Context context;
    SparseArray<ArrayList<ChildCategoryItem>> sparseArray;
    ArrayList<CategoryItem> categoriesArrayList;
    private ArrayList<Integer> selectedCategories = new ArrayList<>();

    public CategoriesExpandAdapter(Context context, SparseArray<ArrayList<ChildCategoryItem>> sparseArray, ArrayList<CategoryItem> categoriesArrayList) {
        this.context = context;
        this.sparseArray = sparseArray;
        this.categoriesArrayList = categoriesArrayList;
    }

    @Override
    public int getGroupCount() {
        return categoriesArrayList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return sparseArray.get(getGroup(groupPosition).getIdCategory()).size();
    }

    @Override
    public CategoryItem getGroup(int groupPosition) {
        return categoriesArrayList.get(groupPosition);
    }

    @Override
    public ChildCategoryItem getChild(int groupPosition, int childPosition) {
        return sparseArray.get(getGroup(groupPosition).getIdCategory()).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return categoriesArrayList.get(groupPosition).getIdCategory();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return sparseArray.get(getGroup(groupPosition).getIdCategory()).get(childPosition).getChildCategoryId();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.category_group_item, null);
        }

        if (selectedCategories.contains(getGroup(groupPosition).getIdCategory())) {
            getGroup(groupPosition).setGroupSelected(true);
        }

        final AQuery aq = new AQuery(convertView);

        aq.id(R.id.categoryNameTextView).text(getGroup(groupPosition).getTitleCategory());
        if (isExpanded)
            aq.id(R.id.arrowImageView).image(R.drawable.ic_category_opened);
        else
            aq.id(R.id.arrowImageView).image(R.drawable.ic_category_closed);

        if (getGroup(groupPosition).getChildCategory().size() == 0) {
            aq.id(R.id.arrowImageView).invisible();
        }
        if (getGroup(groupPosition).getGroupIsSelected())
            aq.id(R.id.checkboxGroupImageView).image(R.drawable.ic_group_checked);
        else
            aq.id(R.id.checkboxGroupImageView).image(R.drawable.ic_group_unchecked);

        aq.id(R.id.checkboxGroupImageView).tag(R.id.tag_group_number, groupPosition);

        aq.id(R.id.checkboxGroupImageView).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int groupPosition = (Integer) v.getTag(R.id.tag_group_number);
                boolean boolValue = !getGroup(groupPosition).getGroupIsSelected();
                getGroup(groupPosition).setGroupSelected(boolValue);
                ArrayList<ChildCategoryItem> array = getGroup(groupPosition).getChildCategory();
                if (array.size() > 0) {
                    for (ChildCategoryItem item : array) {
                        item.setChildSelected(boolValue);
                    }
                }
                notifyDataSetChanged();
            }
        });
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.child_category_item, null);
        }

        if (selectedCategories.contains(getGroup(groupPosition).getIdCategory())) {
            getChild(groupPosition, childPosition).setChildSelected(true);
        }
        final AQuery aq = new AQuery(convertView);

        aq.id(R.id.childCategoryNameTextView).text(getChild(groupPosition, childPosition).getTitleChildCategory());

        if (getChild(groupPosition, childPosition).getChildIsSelected())
            aq.id(R.id.childCheckBox).image(R.drawable.ic_group_checked);
        else
            aq.id(R.id.childCheckBox).image(R.drawable.ic_group_unchecked);

        aq.id(R.id.childCheckBox).tag(R.id.tag_group_number, groupPosition).tag(R.id.tag_child_number, childPosition);

        aq.id(R.id.childCheckBox).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int groupPosition = (Integer) v.getTag(R.id.tag_group_number);
                int childPosition = (Integer) v.getTag(R.id.tag_child_number);
                boolean boolValue = !getChild(groupPosition, childPosition).getChildIsSelected();
                getChild(groupPosition, childPosition).setChildSelected(boolValue);

                if (!boolValue) {
                    getGroup(groupPosition).setGroupSelected(false);
                } else {
                    if (allGroupSelected(groupPosition))
                        getGroup(groupPosition).setGroupSelected(true);
                }
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    private boolean allGroupSelected(int groupPosition) {
        ArrayList<ChildCategoryItem> childArray = getGroup(groupPosition).getChildCategory();
        for (ChildCategoryItem item : childArray) {
            if (!item.getChildIsSelected())
                return false;
        }
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
