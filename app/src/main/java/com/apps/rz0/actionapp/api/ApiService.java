package com.apps.rz0.actionapp.api;

import com.apps.rz0.actionapp.api.model.CommentsWrapper;
import com.apps.rz0.actionapp.api.model.DialogWrapper;
import com.apps.rz0.actionapp.api.model.categories.CategoriesWrapper;
import com.apps.rz0.actionapp.api.model.company.CompanyWrapper;
import com.apps.rz0.actionapp.api.model.register.LegalWrapper;
import com.apps.rz0.actionapp.api.model.register.PartnerWrapper;
import com.apps.rz0.actionapp.api.model.register.UserWrapper;
import com.apps.rz0.actionapp.api.model.shops.ShopFullWrapper;
import com.apps.rz0.actionapp.api.model.shops.ShopWrapper;
import com.apps.rz0.actionapp.api.model.shops.StreetWrapper;
import com.apps.rz0.actionapp.api.model.stock.FilterWrapper;
import com.apps.rz0.actionapp.api.model.stock.FilteredImage;
import com.apps.rz0.actionapp.api.model.stock.PayWrapper;
import com.apps.rz0.actionapp.choose_dialog.CitiesListWrapper;
import com.apps.rz0.actionapp.stocks.StocksWrapper;

import java.util.Map;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;

public interface ApiService {
    @FormUrlEncoded
    @POST("/api/getDialogs")
    void getDialogs(@Field("reg_id") String regId, Callback<DialogWrapper> callback);

    @FormUrlEncoded
    @POST("/api/toggleMessageLike")
    void likeAction(@Field("client_id") String idClient, @Field("message_id") int idMessage, Callback<Response> callback);

    @FormUrlEncoded
    @POST("/api/partnerRegister")
    void partnerRegister(@Field("phone") String phone, @Field("email") String mail,
                         @Field("title") String name, @Field("legal_type") String legalType,
                         @Field("reg_id") String pushId,
                         @Field("inn") String inn, @Field("phone_os") String ops, Callback<PartnerWrapper> callback);

    @GET("/api/getLegalTypes")
    void getLegalTypes(Callback<LegalWrapper> callback);

    @FormUrlEncoded
    @POST("/api/authPartner")
    void authPartner(@Field("email") String mail, @Field("password") String password, Callback<PartnerWrapper> callback);


    @FormUrlEncoded
    @POST("/api/getPartnerShops")
    void getPartnerShops(@Field("partner_id") int partnerId, Callback<ShopWrapper> callback);

    @FormUrlEncoded
    @POST("/api/addPartnerShop")
    void addPartnerShops(@FieldMap Map<String, String> params, Callback<Response> callback);

    @FormUrlEncoded
    @POST("/api/updatePartnerShop")
    void editPartnerShops(@FieldMap Map<String, String> params, Callback<Response> callback);

    @FormUrlEncoded
    @POST("/api/deletePartnerShop")
    void deleteShop(@Field("shop_id") int idShop, @Field("token") String token, Callback<Response> callback);

    @FormUrlEncoded
    @POST("/api/getMessageComments")
    void getComments(@Field("message_id") int idMessage, Callback<CommentsWrapper> callback);

    @FormUrlEncoded
    @POST("/api/commentMessage")
    void sendCommentClient(@Field("client_id") int idClient,
                           @Field("comment") String message, @Field("message_id") int idMessage,
                           Callback<Response> callback);

    @FormUrlEncoded
    @POST("/api/commentMessage")
    void sendCommentPartner(@Field("partner_id") int idPartner,
                            @Field("comment") String message, @Field("message_id") int idMessage,
                            Callback<Response> callback);

    @FormUrlEncoded
    @POST("/api/getFavoriteStocks")
    void getFavoritesStocks(@Field("reg_id") String regId, Callback<StocksWrapper> callback);

    @GET("/api/getAllCities")
    void getAllCities(Callback<CitiesListWrapper> callback);

    @GET("/api/getCategories")
    void getCategories(Callback<CategoriesWrapper> callback);

    @FormUrlEncoded
    @POST("/api/getClientInfo")
    void getClientInfo(@Field("reg_id") String regId, Callback<UserWrapper> callback);

    @FormUrlEncoded
    @POST("/api/getPartnerInfo")
    void getPartnerInfo(@Field("token") String token, Callback<PartnerWrapper> callback);

    @FormUrlEncoded
    @POST("/api/search")
    void search(@Field("key") String query, Callback<StocksWrapper> callback);

    @FormUrlEncoded
    @POST("/api/getStocks")
    void getStocks(@Field("city_id") int cityId, @Field("reg_id") String pushId,
                   @Field("categories") String categories, Callback<StocksWrapper> callback);

    @FormUrlEncoded
    @POST("/api/passwordRecover")
    void recoverPass(@Field("email") String mail, Callback<Response> callback);

    @FormUrlEncoded
    @POST("/api/getCompanyInfo")
    void getCompanyInfo(@Field("partner_id") int partnerId, Callback<CompanyWrapper> callback);

    @FormUrlEncoded
    @POST("/api/getShopInfo")
    void getShopInfo(@Field("shop_id") int idShop, Callback<ShopFullWrapper> callback);

    @FormUrlEncoded
    @POST("/api/addressSuggest")
    void getStreets(@Field("city") String city, @Field("key") String query, Callback<StreetWrapper> callback);

    @GET("/api/getImageFilters")
    void getFilters(Callback<FilterWrapper> callback);

    @FormUrlEncoded
    @POST("/api/filterImage")
    void filterImage(@Field("token") String token, @Field("name") String imageName, @Field("filter") String filterName,
                     Callback<FilteredImage> callback);

    @FormUrlEncoded
    @POST("/api/addStock")
    void addStock(@FieldMap Map<String, String> params, Callback<PayWrapper> callback);
}
