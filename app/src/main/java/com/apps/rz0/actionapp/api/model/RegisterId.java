package com.apps.rz0.actionapp.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Александр on 13.08.2015.
 */
public class RegisterId {
    public User client;
    @SerializedName("code_sended")
    private Boolean codeSended;

    public Boolean isCodeSended() {
        if (codeSended == null)
            return false;
        return codeSended;
    }
}
