package com.apps.rz0.actionapp.company;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.model.company.CompanyItem;
import com.apps.rz0.actionapp.stocks.StockViewHolder;

public class CompanyInfoAdapter extends RecyclerView.Adapter {
    CompanyItem item;
    private final int HEADER = 0;
    private final int STOCK = 1;
    private int widthDisplay;
    private LayoutInflater inflater;
    private ActionBarActivity activity;

    public CompanyInfoAdapter(ActionBarActivity activity, CompanyItem item) {
        this.item = item;
        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        widthDisplay = display.getWidth();
        inflater = LayoutInflater.from(activity);
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        switch (viewType){
            case HEADER : {
                View view = inflater.inflate(R.layout.company_header_item, parent, false);
                holder = new HeaderHolder(view);
                break;
            }
            case STOCK : {
                View view = inflater.inflate(R.layout.item_stock, parent, false);
                holder = new StockViewHolder(activity, view, widthDisplay);
                break;
            }
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == HEADER) {
            ((HeaderHolder) holder).setup(item);
        } else {
            ((StockViewHolder) holder).setup(item.stocks.get(position - 1), position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? HEADER : STOCK;
    }

    @Override
    public int getItemCount() {
        return item.stocks.size() + 1;
    }
}
