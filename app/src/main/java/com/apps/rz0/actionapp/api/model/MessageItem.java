package com.apps.rz0.actionapp.api.model;

import com.google.gson.annotations.SerializedName;

public class MessageItem {
    @SerializedName("message_id")
    public int messageId;
    public String comment;
    public String time;
    public String date;
    @SerializedName("is_user_comment")
    public Boolean isUserComment;
    public String avatar;
}
