package com.apps.rz0.actionapp.company;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.model.company.CompanyItem;
import com.apps.rz0.actionapp.api.model.shops.Coordinates;
import com.apps.rz0.actionapp.api.model.shops.ShopFullItem;
import com.squareup.picasso.Picasso;

public class HeaderHolder extends RecyclerView.ViewHolder {
    TextView name;
    TextView description;
    TextView phone;
    ImageView avatar;
    TextView shops;
    TextView comments;
    TextView likes;
    TextView views;

    public HeaderHolder(View view) {
        super(view);
        name = (TextView) view.findViewById(R.id.name);
        description = (TextView) view.findViewById(R.id.description);
        phone = (TextView) view.findViewById(R.id.phone);
        avatar = (ImageView) view.findViewById(R.id.avatar);
        shops = (TextView) view.findViewById(R.id.shops);
        comments = (TextView) view.findViewById(R.id.comments);
        likes = (TextView) view.findViewById(R.id.likes);
        views = (TextView) view.findViewById(R.id.views);
        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Coordinates item = (Coordinates) v.getTag();
                String title = (String) v.getTag(R.id.tag_name);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:" + item.lat +
                        "," + item.lng + "?q=" + item.lat + "," +
                        item.lng + "(" + title + ")"));
                v.getContext().startActivity(intent);
            }
        });
        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getTag() == null)
                    return;
                Intent intent = new Intent(v.getContext(), CompanyInfoActivity.class);
                intent.putExtra(CompanyInfoActivity.ID_PARTNER, (Integer) v.getTag());
                v.getContext().startActivity(intent);
            }
        });
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + v.getTag()));
                v.getContext().startActivity(intent);
            }
        });
    }

    public void setup(CompanyItem item) {
        name.setText(item.companyName);
        description.setText(item.description);
        Picasso.with(itemView.getContext())
                .load(item.avatar)
                .into(avatar);
        shops.setText(item.shops);
        comments.setText(item.comments);
        likes.setText(item.likes);
        views.setText(item.views);
    }

    public void setup(ShopFullItem item, int partnerId) {
        name.setText(item.title);

        String address = item.address + " " + item.additional + " " + item.city;
        description.setText(address);
        description.setTag(item.coords);
        description.setTag(R.id.tag_name, item.title);
        if (!TextUtils.isEmpty(item.phone)) {
            phone.setText(item.phone);
            phone.setTag(item.phone);
            phone.setVisibility(View.VISIBLE);
        }

        if (TextUtils.isEmpty(item.photo)) {
            avatar.setVisibility(View.GONE);
        } else {
            avatar.setVisibility(View.VISIBLE);
            Picasso.with(itemView.getContext())
                    .load(item.photo)
                    .into(avatar);
            avatar.setTag(partnerId);
        }
        shops.setVisibility(View.GONE);
        comments.setText(item.stocksCount);
        likes.setText(item.likes);
        views.setText(item.views);
    }

}
