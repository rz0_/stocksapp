package com.apps.rz0.actionapp.social;

/**
 * Список видов поддерживаемых соцсетей.
 */
public enum SocialNet {
    FACEBOOK,
    VKONTAKTE,
    ODNOKLASSNIKI
}
