package com.apps.rz0.actionapp.notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Александр on 06.05.2015.
 */
public class RegIDWrapper {
    private Boolean success;
    @SerializedName("client_id")
    private Long clientId;

    public Long getClientId() {
        return clientId;
    }
}
