package com.apps.rz0.actionapp.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.StocksApplication;
import com.apps.rz0.actionapp.choose_dialog.CityItem;
import com.apps.rz0.actionapp.notifications.NotificationHelper;
import com.apps.rz0.actionapp.utils.StaticStrings;
import com.apps.rz0.actionapp.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Контроллер для получения данных с сервера
 */
public class ControllerServerAPI {
    ServerAPI api;
    Context context;
    AQuery aq;

    public ControllerServerAPI(Context context) {
        this.context = context;
        aq = new AQuery(context);
        api = new ServerAPI(context);
    }

    public void getCityByCoords(double lat, double lon, CallbackApi callback) {
        api.getCityByCoords(lat, lon, callback);
    }

    public void getCategoriesList(CallbackApi callback) {
        api.getCategoriesList(callback);
    }

    public void getStocksList(final CallbackApi callback) {
        Gson gson = new Gson();
        SharedPreferences sharedPreferences = context.getSharedPreferences(StaticStrings.CURRENT_SHARED_PREF, Context.MODE_PRIVATE);
        final ArrayList<Integer> selectedCategories = gson.fromJson(sharedPreferences.getString(StaticStrings.SELECTED_ACTION, "[]"), new TypeToken<List<Integer>>() {
        }.getType());

        int idCity = 1;
        if (Utils.getRegUser(aq.getContext()) != null){
            idCity = Utils.getRegUser(aq.getContext()).getIdCity();
        }
        if(Utils.getPartner(aq.getContext()) != null) {
            idCity = 1;
        }

        String ids = "";
        for (Integer id : selectedCategories)
            ids += String.valueOf(id) + ",";

        if (ids.length() > 0)
            ids = ids.substring(0, ids.length() - 1);

        Log.d("StockFragment", "idCity = " + idCity + " ids " + ids);

        api.getStocksList(idCity, ids, callback);
    }

    public void saveChoosedCity(long cityId, CallbackApi callback) {
        SharedPreferences preferences = NotificationHelper.getGCMPreferences(context);
        String regId = preferences.getString(NotificationHelper.PROPERTY_REG_ID, "");
        if (TextUtils.isEmpty(regId))
            return;

        api.saveChoosedCity(cityId, regId, callback);
    }

    public void saveCategories(ArrayList<Integer> idsCategories, CallbackApi callback) {
        SharedPreferences preferences = NotificationHelper.getGCMPreferences(context);
        String regId = preferences.getString(NotificationHelper.PROPERTY_REG_ID, "");
        if (TextUtils.isEmpty(regId))
            return;

        String ids = "";
        for (Integer item : idsCategories)
            ids += String.valueOf(item) + ",";

        if (!TextUtils.isEmpty(ids))
            ids = ids.substring(0, ids.length() - 1);

        api.saveCategories(ids, regId, callback);
    }

    public void savePhone(String phone, CallbackApi callback) {
        SharedPreferences preferences = NotificationHelper.getGCMPreferences(context);
        String regId = preferences.getString(NotificationHelper.PROPERTY_REG_ID, "");
        if (TextUtils.isEmpty(regId))
            return;

        api.savePhone(phone, regId, callback);
    }

    public void savePushStatus(boolean isPushEnabled, CallbackApi callback) {
        SharedPreferences preferences = NotificationHelper.getGCMPreferences(context);
        String regId = preferences.getString(NotificationHelper.PROPERTY_REG_ID, "");
        if (TextUtils.isEmpty(regId))
            return;

        int isPush = isPushEnabled ? 1 : 0;
        api.savePushStatus(isPush, regId, callback);
    }

    public void saveSmsStatus(boolean isSmsEnabled, CallbackApi callback) {
        SharedPreferences preferences = NotificationHelper.getGCMPreferences(context);
        String regId = preferences.getString(NotificationHelper.PROPERTY_REG_ID, "");
        if (TextUtils.isEmpty(regId))
            return;

        int isSms = isSmsEnabled ? 1 : 0;
        api.saveSmsStatus(isSms, regId, callback);
    }
}
