package com.apps.rz0.actionapp.api;

import android.text.TextUtils;

import com.apps.rz0.actionapp.StocksApplication;
import com.apps.rz0.actionapp.api.model.CommentItem;
import com.apps.rz0.actionapp.api.model.CommentsWrapper;
import com.apps.rz0.actionapp.api.model.DialogWrapper;
import com.apps.rz0.actionapp.api.model.MessageItem;
import com.apps.rz0.actionapp.api.model.User;
import com.apps.rz0.actionapp.api.model.categories.CategoriesWrapper;
import com.apps.rz0.actionapp.api.model.categories.CategoryItem;
import com.apps.rz0.actionapp.api.model.company.CompanyItem;
import com.apps.rz0.actionapp.api.model.company.CompanyWrapper;
import com.apps.rz0.actionapp.api.model.register.LegalType;
import com.apps.rz0.actionapp.api.model.register.LegalWrapper;
import com.apps.rz0.actionapp.api.model.register.PartnerWrapper;
import com.apps.rz0.actionapp.api.model.register.UserWrapper;
import com.apps.rz0.actionapp.api.model.shops.ShopFullItem;
import com.apps.rz0.actionapp.api.model.shops.ShopFullWrapper;
import com.apps.rz0.actionapp.api.model.shops.ShopItem;
import com.apps.rz0.actionapp.api.model.shops.ShopWrapper;
import com.apps.rz0.actionapp.api.model.shops.StreetWrapper;
import com.apps.rz0.actionapp.api.model.stock.FilterItem;
import com.apps.rz0.actionapp.api.model.stock.FilterWrapper;
import com.apps.rz0.actionapp.api.model.stock.FilteredImage;
import com.apps.rz0.actionapp.api.model.stock.PayWrapper;
import com.apps.rz0.actionapp.choose_dialog.CitiesListWrapper;
import com.apps.rz0.actionapp.choose_dialog.CityItem;
import com.apps.rz0.actionapp.stocks.StockItem;
import com.apps.rz0.actionapp.stocks.StocksWrapper;
import com.apps.rz0.actionapp.utils.StaticStrings;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;


public enum Api {
    get;

    ApiService service;

    Api() {
        OkHttpClient okHttpClient = new OkHttpClient();
        File cacheDir = new File(StocksApplication.getAppContext().getCacheDir(), UUID.randomUUID().toString());
        Cache cache = new Cache(cacheDir, 30L * 1024 * 1024);
        okHttpClient.setCache(cache);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(StaticStrings.SERVER_URL_2)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setErrorHandler(new CustomErrorHandler())
                .setClient(new OkClient(okHttpClient))
                .build();
        service = restAdapter.create(ApiService.class);
    }

    public interface OnDataGet<T> {
        void getData(T success);
    }

    public void getDialogs(String idPush, final OnDataGet<ArrayList<MessageItem>> cb) {
        service.getDialogs(idPush, new Callback<DialogWrapper>() {
            @Override
            public void success(DialogWrapper arrayListBaseAnswer, Response response) {
                cb.getData(arrayListBaseAnswer.dialogs);
            }

            @Override
            public void failure(RetrofitError error) {
                cb.getData(null);
            }
        });
    }

    public void likeAction(String idUser, int idMessage) {
        service.likeAction(idUser, idMessage, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                int i = 0;
            }

            @Override
            public void failure(RetrofitError error) {
                int i = 0;
            }
        });
    }

    public void registerPartner(String phone, String mail, String title, String legalType,
                                String pushId, String inn, Callback<PartnerWrapper> callback) {
        service.partnerRegister(phone, mail, title, legalType, pushId, inn, "android", callback);
    }

    public void getLegalTypes(final OnDataGet<ArrayList<LegalType>> cb) {
        service.getLegalTypes(new Callback<LegalWrapper>() {
            @Override
            public void success(LegalWrapper legalWrapper, Response response) {
                cb.getData(legalWrapper.legalTypes);
            }

            @Override
            public void failure(RetrofitError error) {
                cb.getData(null);
            }
        });
    }

    public void authPartner(String mail, String password, Callback<PartnerWrapper> callback) {
        service.authPartner(mail, password, callback);
    }

    private long lastTokenUpdate = 0;

    public void updateToken(String mail, String password) {
        if (TextUtils.isEmpty(mail) || TextUtils.isEmpty(password))
            return;
        if (System.currentTimeMillis() - 30 * 60 * 1000 < lastTokenUpdate)
            return;
        service.authPartner(mail, password, new Callback<PartnerWrapper>() {
            @Override
            public void success(PartnerWrapper partnerWrapper, Response response) {
                StocksApplication.partnerToken = partnerWrapper.token;
                lastTokenUpdate = System.currentTimeMillis();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void getShops(int idPartner, final OnDataGet<ArrayList<ShopItem>> callback) {
        service.getPartnerShops(idPartner, new Callback<ShopWrapper>() {
            @Override
            public void success(ShopWrapper shopWrapper, Response response) {
                callback.getData(shopWrapper.shops);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.getData(null);
            }
        });
    }

    public void addShop(Map<String, String> params, Callback<Response> callback) {
        service.addPartnerShops(params, callback);
    }

    public void editShop(Map<String, String> params, Callback<Response> callback) {
        service.editPartnerShops(params, callback);
    }

    public void deleteShop(int idShop, Callback<Response> callback) {
        service.deleteShop(idShop, StocksApplication.partnerToken, callback);
    }

    public void getComments(int idMessage, final OnDataGet<ArrayList<CommentItem>> callback) {
        service.getComments(idMessage, new Callback<CommentsWrapper>() {
            @Override
            public void success(CommentsWrapper commentsWrapper, Response response) {
                callback.getData(commentsWrapper.getComments());
            }

            @Override
            public void failure(RetrofitError error) {
                callback.getData(null);
            }
        });
    }

    public void addComment(int idClient, int idPartner, int idMessage, String comment, final OnDataGet<String> callback) {
        Callback<Response> callback1 = new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                callback.getData("");
            }

            @Override
            public void failure(RetrofitError error) {
                callback.getData("");
            }
        };
        if (idPartner == 0)
            service.sendCommentClient(idClient, comment, idMessage, callback1);
        else
            service.sendCommentPartner(idPartner, comment, idMessage, callback1);
    }

    public void getFavoritesStocks(String regId, final OnDataGet<ArrayList<StockItem>> callback) {
        service.getFavoritesStocks(regId, new Callback<StocksWrapper>() {
            @Override
            public void success(StocksWrapper stocksWrapper, Response response) {
                callback.getData(stocksWrapper.getStocks());
            }

            @Override
            public void failure(RetrofitError error) {
                callback.getData(null);
            }
        });
    }

    public void getAllCities(final OnDataGet<ArrayList<CityItem>> callback) {
        service.getAllCities(new Callback<CitiesListWrapper>() {
            @Override
            public void success(CitiesListWrapper citiesListWrapper, Response response) {
                callback.getData(citiesListWrapper.getCitiesList());
            }

            @Override
            public void failure(RetrofitError error) {
                callback.getData(null);
            }
        });
    }

    public void getClientInfo(String regId, final OnDataGet<User> callback) {
        service.getClientInfo(regId, new Callback<UserWrapper>() {
            @Override
            public void success(UserWrapper userWrapper, Response response) {
                callback.getData(userWrapper.client);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.getData(null);
            }
        });
    }

    public void getPartnerInfo(final Callback<PartnerWrapper> callback) {
        service.getPartnerInfo(StocksApplication.partnerToken, callback);
    }

    public void searchItems(String query, final OnDataGet<ArrayList<StockItem>> callback) {
        service.search(query, new Callback<StocksWrapper>() {
            @Override
            public void success(StocksWrapper stocksWrapper, Response response) {
                callback.getData(stocksWrapper.getStocks());
            }

            @Override
            public void failure(RetrofitError error) {
                callback.getData(null);
            }
        });
    }

    public void getStocks(int cityId, String pushId, String categories, final OnDataGet<ArrayList<StockItem>> callback) {
        service.getStocks(cityId, pushId, categories, new Callback<StocksWrapper>() {
            @Override
            public void success(StocksWrapper stocksWrapper, Response response) {
                callback.getData(stocksWrapper.getStocks());
            }

            @Override
            public void failure(RetrofitError error) {
                callback.getData(null);
            }
        });
    }

    public void recoverPass(String mail, Callback<Response> callback) {
        service.recoverPass(mail, callback);
    }

    public void getCompanyInfo(int idPartner, final OnDataGet<CompanyItem> callback) {
        service.getCompanyInfo(idPartner, new Callback<CompanyWrapper>() {
            @Override
            public void success(CompanyWrapper wrapper, Response response) {
                callback.getData(wrapper.partner);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.getData(null);
            }
        });
    }

    public void getShopInfo(int idShop, final OnDataGet<ShopFullItem> callback) {
        service.getShopInfo(idShop, new Callback<ShopFullWrapper>() {
            @Override
            public void success(ShopFullWrapper shopFullWrapper, Response response) {
                callback.getData(shopFullWrapper.shop);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.getData(null);
            }
        });
    }

    public void getStreets(String city, String query, final OnDataGet<ArrayList<String>> callback) {
        service.getStreets(city, query, new Callback<StreetWrapper>() {
            @Override
            public void success(StreetWrapper streetWrapper, Response response) {
                callback.getData(streetWrapper.suggests);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.getData(null);
            }
        });
    }

    public void getFilters(final OnDataGet<ArrayList<FilterItem>> callback) {
        service.getFilters(new Callback<FilterWrapper>() {
            @Override
            public void success(FilterWrapper filterWrapper, Response response) {
                callback.getData(filterWrapper.filters);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.getData(null);
            }
        });
    }

    public void filterImage(String imageName, String filterName, final OnDataGet<FilteredImage> callback) {
        service.filterImage(StocksApplication.partnerToken, imageName, filterName, new Callback<FilteredImage>() {
            @Override
            public void success(FilteredImage filteredImage, Response response) {
                callback.getData(filteredImage);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.getData(null);
            }
        });
    }

    public void addStock(Map<String, String> params, final Callback<PayWrapper> callback) {
        service.addStock(params, callback);
    }

    public void getCategories(final OnDataGet<ArrayList<CategoryItem>> callback) {
        service.getCategories(new Callback<CategoriesWrapper>() {
            @Override
            public void success(CategoriesWrapper categoriesWrapper, Response response) {
                callback.getData(categoriesWrapper.categories);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.getData(null);
            }
        });
    }
}
