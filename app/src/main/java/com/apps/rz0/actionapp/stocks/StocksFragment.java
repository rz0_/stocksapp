package com.apps.rz0.actionapp.stocks;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.categories.CategoriesActivity;
import com.apps.rz0.actionapp.utils.StaticStrings;
import com.apps.rz0.actionapp.utils.Utils;

import java.util.ArrayList;

/**
 * Фрагмент для списка акций
 */
public class StocksFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener {

    AQuery aq;
    RecyclerView stocksRecyclerView;
    SwipeRefreshLayout swipeLayout;
    private long currentSearchTime;
    private Handler handlerSearch;

    BroadcastReceiver updateRecyclerView = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refresh();
        }
    };
    private SearchView mSearchView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stocks, container, false);
        setHasOptionsMenu(true);
        aq = new AQuery(view);
        handlerSearch = new Handler();
        stocksRecyclerView = (RecyclerView) view.findViewById(R.id.stocksRecyclerView);
        stocksRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeLayout.setEnabled(false);
        swipeLayout.setOnRefreshListener(this);
        stocksRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                Log.d("STATE ", " " + newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                swipeLayout.setEnabled(((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition() == 0);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
        getActivity().registerReceiver(updateRecyclerView, new IntentFilter(StaticStrings.UPDATE_DRAWER_RECEIVER));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(updateRecyclerView);
    }

    private void refresh() {
        Api.get.getStocks(Utils.getCityId(getActivity()), Utils.getRegPushId(getActivity()),
                Utils.getSelectedCategories(getActivity()), new Api.OnDataGet<ArrayList<StockItem>>() {
                    @Override
                    public void getData(ArrayList<StockItem> success) {
                        setAdapter(success);
                        swipeLayout.setRefreshing(false);
                    }
                });
    }

    private void setAdapter(ArrayList<StockItem> success) {
        stocksRecyclerView.setVisibility(View.VISIBLE);
        aq.id(R.id.progressBar).gone();
        if (success != null && success.size() > 0) {
            aq.id(R.id.empty_stocks).gone();
            if (stocksRecyclerView.getAdapter() == null)
                stocksRecyclerView.setAdapter(new StocksAdapter((ActionBarActivity) getActivity(), success));
            else {
                ((StocksAdapter) stocksRecyclerView.getAdapter()).stockItems = success;
                stocksRecyclerView.getAdapter().notifyDataSetChanged();
            }
        } else {
            aq.id(R.id.empty_stocks).visible();
            stocksRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        swipeLayout.setRefreshing(true);
        if (isVisible())
            refresh();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_stocks, menu);
        MenuItem searchMenuItem = menu.findItem(R.id.menu_search);
        mSearchView = (SearchView) searchMenuItem.getActionView();

        initSearchView();

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void initSearchView() {
        mSearchView.setIconifiedByDefault(false);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        if (searchManager != null) {
            SearchableInfo info = searchManager.getSearchableInfo(getActivity().getComponentName());
            mSearchView.setSearchableInfo(info);
        }

        mSearchView.setOnQueryTextListener(this);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mSearchView.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        mSearchView.clearFocus();
//        searchItems("", 100);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_parameters) {
            startActivity(new Intent(getActivity(), CategoriesActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    private void searchItems(final String query) {
        if (TextUtils.isEmpty(query)) {
            refresh();
            return;
        }
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Api.get.searchItems(query, new Api.OnDataGet<ArrayList<StockItem>>() {
                    @Override
                    public void getData(ArrayList<StockItem> success) {
                        setAdapter(success);
                    }
                });
            }
        };

        handlerSearch.postDelayed(runnable, 1000);

    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        searchItems(query);
        if (currentSearchTime + 1000 > System.currentTimeMillis()) {
            handlerSearch.removeCallbacksAndMessages(null);
            searchItems(query);
        }
        currentSearchTime = System.currentTimeMillis();
        return false;
    }
}
