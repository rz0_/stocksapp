package com.apps.rz0.actionapp.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Александр on 17.08.2015.
 */
public class SaveCityResponce {
    boolean success;
    @SerializedName("choose_city")
    boolean chooseCity;
    private User client;

    public User getClient() {
        return client;
    }

    public boolean isChooseCity() {
        return chooseCity;
    }
}
