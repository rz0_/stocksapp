package com.apps.rz0.actionapp.choose_dialog;

import com.google.gson.annotations.SerializedName;

/**
 * Один элемент-город
 */
public class CityItem {
    @SerializedName("city_id")
    private Integer cityId;
    private String title;

    public Integer getCityId() {
        return cityId;
    }

    public String getCityTitle() {
        return title;
    }

    @Override
    public String toString() {
        return title;
    }
}
