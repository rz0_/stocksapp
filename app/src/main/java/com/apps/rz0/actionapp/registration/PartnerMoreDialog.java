package com.apps.rz0.actionapp.registration;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.MainActivity;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.StocksApplication;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.CallbackApi2;
import com.apps.rz0.actionapp.api.model.register.PartnerItem;
import com.apps.rz0.actionapp.api.model.register.PartnerWrapper;
import com.apps.rz0.actionapp.choose_dialog.CityItem;
import com.apps.rz0.actionapp.utils.StaticStrings;
import com.apps.rz0.actionapp.utils.ThemesUtils;
import com.apps.rz0.actionapp.utils.Utils;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class PartnerMoreDialog extends DialogFragment {
    public static String TAG = "partner_more";
    public static final int GALLERY_CODE = 100;
    private String picturePath;

    private EditText title;
    private EditText description;
    private Spinner citiesSpinner;
    private ArrayList<CityItem> cities;
    private AQuery aq;
    private int userCityId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.more_register_layout, container, false);
        aq = new AQuery(view);
        Api.get.updateToken(Utils.getPartnerLogin(getActivity()), Utils.getPartnerPass(getActivity()));
        title = aq.id(R.id.title_company).getEditText();
        description = aq.id(R.id.description_company).getEditText();
        citiesSpinner = aq.id(R.id.cities_spinner).getSpinner();

        if (getArguments().getBoolean(MainActivity.IS_EDIT_PROFILE)) {
            aq.id(R.id.main_layout).gone();
            getPartnerInfo();
        } else {
            aq.id(R.id.progress).gone();
            aq.id(R.id.toolbar_view_title).text("Вход для партнеров");
        }

        aq.id(R.id.choose_photo_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = MediaStore.Images.Media.INTERNAL_CONTENT_URI;
                Intent intent = new Intent(Intent.ACTION_PICK, uri);
                intent.setType("image/*");
                startActivityForResult(intent, GALLERY_CODE);
            }
        });

        Api.get.getAllCities(new Api.OnDataGet<ArrayList<CityItem>>() {
            @Override
            public void getData(ArrayList<CityItem> success) {
                if (success == null || success.size() == 0)
                    return;
                cities = success;

                citiesSpinner.setAdapter(
                        new CityArrayAdapter(getActivity(), R.layout.spinner_main_layout, success));
                if (getArguments().getBoolean(MainActivity.IS_EDIT_PROFILE, false))
                    setCityAdapter(userCityId);
            }
        });

        aq.id(R.id.save_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkFields())
                    aq.id(R.id.progress).visible();
                fillPartner();
            }
        });

        aq.id(R.id.arrow_home).visible().clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        ThemesUtils.setToolbarColor(getActivity(), aq.id(R.id.toolbar_view).getView());

        return view;
    }

    private void setCityAdapter(int userCityId) {
        for (int i = 0; i < cities.size(); i++) {
            CityItem item = cities.get(i);
            if (item.getCityId() == userCityId) {
                citiesSpinner.setSelection(i);
                break;
            }
        }
    }


    private void getPartnerInfo() {
        Api.get.getPartnerInfo(new Callback<PartnerWrapper>() {
            @Override
            public void success(PartnerWrapper partnerWrapper, Response response) {
                aq.id(R.id.main_layout).visible();
                aq.id(R.id.progress).gone();
                PartnerItem partner = partnerWrapper.partner;
                if (partner == null)
                    return;
                aq.id(R.id.toolbar_view_title).text("Профиль");
                aq.id(R.id.choose_photo_description).gone();
                aq.id(R.id.choose_avatar_button).image(partner.avatar);
                title.setText(partner.companyName);
                description.setText(partner.description);
                if (cities != null && citiesSpinner.getAdapter() != null) {
                    setCityAdapter(partner.cityId);
                } else {
                    userCityId = partner.cityId;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void fillPartner() {
        StocksApplication.registerApi.fillPartner(title.getText().toString(), description.getText().toString(),
                picturePath, citiesSpinner.getSelectedItemId(), new CallbackApi2<PartnerWrapper>() {
                    @Override
                    public void onSuccess(PartnerWrapper partnerWrapper) {
                        aq.id(R.id.progress).gone();
                        Utils.clearUsers(getActivity());
                        Utils.savePartner(getActivity(), partnerWrapper.partner);
                        Utils.dismissDialog(PartnerMoreDialog.this);
                    }

                    @Override
                    public void onError(String error) {
                        aq.id(R.id.progress).gone();
                        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                    }
                });
    }

    private boolean checkFields() {
        title.setError(null);
        description.setError(null);
        if (TextUtils.isEmpty(title.getText())) {
            title.setError("Введите название организации");
            return false;
        }
        if (TextUtils.isEmpty(description.getText())) {
            description.setError("Введите описание организации");
            return false;
        }
        return true;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GALLERY_CODE && data != null) {
            loadAvatar(data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loadAvatar(Intent data) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        picturePath = cursor.getString(columnIndex);
        cursor.close();
        aq.id(R.id.choose_photo_description).gone();
        aq.id(R.id.choose_avatar_button).getImageView().setImageBitmap(BitmapFactory.decodeFile(picturePath));
    }

    @Override
    public void dismiss() {
        super.dismiss();
        getActivity().sendBroadcast(new Intent(StaticStrings.UPDATE_DRAWER_RECEIVER));
    }
}
