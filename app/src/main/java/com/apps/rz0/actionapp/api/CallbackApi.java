package com.apps.rz0.actionapp.api;

/**
 * Callback для работы с сервером
 */
public interface CallbackApi {
    public void onSuccess(String object);
    public void onError(String error);
}
