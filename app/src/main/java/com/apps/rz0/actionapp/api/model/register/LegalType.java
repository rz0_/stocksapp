package com.apps.rz0.actionapp.api.model.register;

import com.google.gson.annotations.SerializedName;

public class LegalType {
    @SerializedName("legal_type_id")
    public Integer legalTypeId;
    public String title;

    @Override
    public String toString() {
        return title;
    }
}
