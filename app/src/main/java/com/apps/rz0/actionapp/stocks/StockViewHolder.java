package com.apps.rz0.actionapp.stocks;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.comments.CommentActivity;
import com.apps.rz0.actionapp.shops.ShopInfoActivity;
import com.squareup.picasso.Picasso;

public class StockViewHolder extends RecyclerView.ViewHolder {

    ImageView companyLogo;
    ImageView imageStock;
    TextView timeStock;
    TextView shortDescription;
    TextView stockSize;
    View likeButton;
    TextView likeCount;
    ImageView likeLogo;
    ImageView share;
    TextView commentCount;
    TextView fullDescription;

    public StockViewHolder(final ActionBarActivity activity, View itemView, int widthDisplay) {
        super(itemView);
        companyLogo = (ImageView) itemView.findViewById(R.id.company_logo_stock);
        imageStock = (ImageView) itemView.findViewById(R.id.image_stock);
        imageStock.getLayoutParams().height = widthDisplay;

        timeStock = (TextView) itemView.findViewById(R.id.time_stock);
        shortDescription = (TextView) itemView.findViewById(R.id.short_description_stock);
        stockSize = (TextView) itemView.findViewById(R.id.size_percent_stock);
        likeCount = (TextView) itemView.findViewById(R.id.like_counter_stock);
        share = (ImageView) itemView.findViewById(R.id.share_button);
        likeButton = itemView.findViewById(R.id.like_button);
        likeLogo = (ImageView) itemView.findViewById(R.id.like_logo);
        commentCount = (TextView) itemView.findViewById(R.id.comment_counter_stock);
        fullDescription = (TextView) itemView.findViewById(R.id.full_description_stock);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareDialog dialog = new ShareDialog(activity, (StockItem) v.getTag());
                dialog.show();
            }
        });

        commentCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), CommentActivity.class);
                intent.putExtra(CommentActivity.ID_MESSAGE, (Integer) v.getTag(R.id.tag_id));
                v.getContext().startActivity(intent);
            }
        });

        companyLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getTag() == null)
                    return;
                Intent intent = new Intent(v.getContext(), ShopInfoActivity.class);
                intent.putExtra(ShopInfoActivity.ID_SHOP, (Integer) v.getTag());
                v.getContext().startActivity(intent);
            }
        });
    }

    public void setup(StockItem item, int position) {
        shortDescription.setText(item.getShort_descr());
        if (!TextUtils.isEmpty(item.getImage())) {
            Picasso.with(itemView.getContext())
                    .load(item.getImage())
                    .into(imageStock);
        }

        fullDescription.setText(item.getContent());
        stockSize.setText(item.getDiscount());
        if (item.getLikes() != null) {
            likeCount.setText(String.valueOf(item.getLikes()));
            likeCount.setVisibility(View.VISIBLE);
            likeButton.setVisibility(View.VISIBLE);
        } else {
            likeCount.setVisibility(View.GONE);
            likeButton.setVisibility(View.GONE);
        }
        if (item.getCommentsNum() != null) {
            commentCount.setText(String.valueOf(item.getCommentsNum()));
            commentCount.setVisibility(View.VISIBLE);
        } else {
            commentCount.setVisibility(View.GONE);
        }
        timeStock.setText(item.getDaysLeft());
        if (!TextUtils.isEmpty(item.getCompanyAvatar())) {
            Picasso.with(itemView.getContext())
                    .load(item.getCompanyAvatar())
                    .into(companyLogo);
        }
        if (item.idShop != 0)
            companyLogo.setTag(item.idShop);

        likeButton.setTag(R.id.tag_id, item);
        likeButton.setTag(R.id.tag_position, position);
        itemView.setTag(R.id.tag_id, item);
        itemView.setTag(R.id.tag_position, position);
        if (!item.isLiked())
            likeLogo.setImageResource(R.drawable.ic_hearth_stock_item);
        else
            likeLogo.setImageResource(R.drawable.ic_hearth_stock_item_pressed);


        commentCount.setTag(R.id.tag_id, item.getStockId());

        share.setTag(item);
    }
}

