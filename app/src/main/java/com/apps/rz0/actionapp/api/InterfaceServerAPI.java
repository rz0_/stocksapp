package com.apps.rz0.actionapp.api;

import com.apps.rz0.actionapp.choose_dialog.CityItem;

import java.util.ArrayList;

/**
 * Интерфейс для работы с сервером
 */
public interface InterfaceServerAPI {
    public void getCityByCoords(double lat, double lon, CallbackApi callback);

    public void getCategoriesList(CallbackApi callback);

    public void getStocksList(long cityId, String categories, final CallbackApi callback);

    public void saveChoosedCity(long cityId, String userId, CallbackApi callback);

    public void saveCategories(String idsCategories, String userId, CallbackApi callback);

    public void savePhone(String phone, String userId, CallbackApi callback);

    public void savePushStatus(int isPushEnabled, String userId, CallbackApi callback);

    public void saveSmsStatus(int isSmsEnabled, String userId, CallbackApi callback);
}
