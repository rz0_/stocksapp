package com.apps.rz0.actionapp;


import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.CallbackApi2;
import com.apps.rz0.actionapp.choose_dialog.CitiesListWrapper;
import com.apps.rz0.actionapp.choose_dialog.CityItem;
import com.apps.rz0.actionapp.choose_dialog.CityListAdapter;
import com.apps.rz0.actionapp.api.CallbackApi;
import com.apps.rz0.actionapp.stocks.StocksFragment;
import com.apps.rz0.actionapp.utils.StaticStrings;
import com.google.gson.Gson;

import java.util.ArrayList;


/**
 * Фрагмент для выбора моего города
 */
public class MyCityFragment extends Fragment {

    AQuery aq;
    LocationManager locationManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_city, container, false);
        SharedPreferences preferences = getActivity().getSharedPreferences(StaticStrings.CURRENT_SHARED_PREF, Context.MODE_PRIVATE);
        aq = new AQuery(view);
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        aq.id(R.id.currentCityView).gone();
        aq.id(R.id.anotherCitiesTextView).gone();
        aq.id(R.id.chooseCityListView).gone();
        aq.id(R.id.progressBar).visible();

        aq.id(R.id.cityNameTextView).text(preferences.getString(StaticStrings.CURRENT_CITY_NAME, "Москва"));

        Api.get.getAllCities(new Api.OnDataGet<ArrayList<CityItem>>() {
            @Override
            public void getData(ArrayList<CityItem> success) {
                aq.id(R.id.currentCityView).visible();
                aq.id(R.id.anotherCitiesTextView).visible();
                aq.id(R.id.chooseCityListView).visible();
                aq.id(R.id.progressBar).gone();;
                aq.id(R.id.chooseCityListView).adapter(new CityListAdapter(getActivity(), success));
            }
        });

        aq.id(R.id.chooseCityListView).getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String titleCity = (String) view.findViewById(R.id.cityNameTextView).getTag(R.id.tag_name);
                showConfirmDialog(id, titleCity);
            }
        });

        return view;
    }

    private void showConfirmDialog(final long idCity, final String nameCity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.confirm_choosed_city_layout, null);

        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();
        view.findViewById(R.id.yesButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(StaticStrings.CURRENT_SHARED_PREF, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong(StaticStrings.CURRENT_CITY_ID, idCity);
                editor.putString(StaticStrings.CURRENT_CITY_NAME, nameCity);
                aq.id(R.id.cityNameTextView).text(nameCity);
                StocksApplication.serverController.saveChoosedCity(idCity, new CallbackApi() {
                    @Override
                    public void onSuccess(String json) {
                        StocksFragment fragment = ((MainActivity) getActivity()).stocksFragment;
                        if(fragment != null)
                            ((MainActivity)getActivity()).setFragment(fragment, 2, MainActivity.STOCKS_MENU);
                    }

                    @Override
                    public void onError(String error) {
                        StocksFragment fragment = ((MainActivity) getActivity()).stocksFragment;
                        if(fragment != null)
                            ((MainActivity)getActivity()).setFragment(fragment, 2, MainActivity.STOCKS_MENU);
                    }
                });
                editor.apply();
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.noButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}
