package com.apps.rz0.actionapp.shops;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.model.company.CompanyItem;
import com.apps.rz0.actionapp.api.model.shops.ShopFullItem;
import com.apps.rz0.actionapp.company.CompanyInfoAdapter;
import com.apps.rz0.actionapp.utils.ThemesUtils;

public class ShopInfoActivity extends ActionBarActivity {

    public static final String ID_SHOP = "id_shop";
    private AQuery aq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        aq = new AQuery(this);
        aq.id(R.id.progress).visible();

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_button);
        getSupportActionBar().setTitle("Информация о магазине");
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.company_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(ShopInfoActivity.this));
        ThemesUtils.setToolbarColor(this);

//        Api.get.getShopInfo(57, new Api.OnDataGet<ShopFullItem>() {
        Api.get.getShopInfo(getIntent().getIntExtra(ID_SHOP, -1), new Api.OnDataGet<ShopFullItem>() {
            @Override
            public void getData(ShopFullItem success) {
                aq.id(R.id.progress).gone();
                if (success == null)
                    return;
                recyclerView.setAdapter(new ShopInfoAdapter(ShopInfoActivity.this, success));
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
