package com.apps.rz0.actionapp;

import android.app.Application;
import android.content.Context;
import android.provider.Settings;

import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.ControllerServerAPI;
import com.apps.rz0.actionapp.api.model.User;
import com.apps.rz0.actionapp.api.register.RegisterApi;
import com.apps.rz0.actionapp.api.register.StockApi;
import com.apps.rz0.actionapp.social.SocialSDK;
import com.apps.rz0.actionapp.utils.Utils;

public class StocksApplication extends Application {

    public static ControllerServerAPI serverController;
    public static RegisterApi registerApi;
    public static StockApi stockApi;
    public static User currentUser;
    public static String partnerToken;

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        serverController = new ControllerServerAPI(getApplicationContext());
        registerApi = new RegisterApi(getApplicationContext());
        stockApi = new StockApi(getApplicationContext());
        currentUser = Utils.getRegUser(getApplicationContext());
        mContext = getApplicationContext();
        Api.get.updateToken(Utils.getPartnerLogin(getApplicationContext()), Utils.getPartnerPass(getApplicationContext()));
        SocialSDK.initialize(this);

        Settings.System.putInt(getApplicationContext().getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0);
    }

    public static Context getAppContext() {
        return StocksApplication.mContext;
    }
}
