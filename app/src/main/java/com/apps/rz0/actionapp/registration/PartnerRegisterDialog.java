package com.apps.rz0.actionapp.registration;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.model.register.LegalType;
import com.apps.rz0.actionapp.api.model.register.PartnerWrapper;
import com.apps.rz0.actionapp.utils.ThemesUtils;
import com.apps.rz0.actionapp.utils.Utils;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Александр on 05.07.2015.
 */
public class PartnerRegisterDialog extends DialogFragment {
    public static final String TAG = "partner_register_dialog";
    private EditText name;
    private EditText inn;
    private EditText phone;
    private EditText mail;
    private Spinner legalSpinner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.partner_register_layout, container, false);
        AQuery aq = new AQuery(view);
        legalSpinner = aq.id(R.id.legal_spinner).getSpinner();
        legalSpinner.setEnabled(false);
        Api.get.getLegalTypes(new Api.OnDataGet<ArrayList<LegalType>>() {
            @Override
            public void getData(ArrayList<LegalType> success) {
                if (success == null || success.size() == 0)
                    return;
                legalSpinner.setEnabled(true);
                legalSpinner.setAdapter(new LegalTypesArrayAdapter(getActivity(), R.layout.spinner_main_layout, success));
            }
        });

        aq.id(R.id.toolbar_view_title).text("Регистрация");
        aq.id(R.id.arrow_home).visible().clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        aq.id(R.id.register_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerPartner();
            }
        });

        name = aq.id(R.id.name_partner).getEditText();
        inn = aq.id(R.id.inn_partner).getEditText();
        phone = aq.id(R.id.phone_partner).getEditText();
        mail = aq.id(R.id.email_partner).getEditText();

        ThemesUtils.setToolbarColor(getActivity(), aq.id(R.id.toolbar_view).getView());

        return view;
    }

    private void registerPartner() {
        if (checkFields())
            Api.get.registerPartner(phone.getText().toString(), mail.getText().toString(), name.getText().toString(),
                    ((LegalType) legalSpinner.getSelectedItem()).legalTypeId.toString(),
                    Utils.getRegPushId(getActivity()),
                    inn.getText().toString(), new Callback<PartnerWrapper>() {
                        @Override
                        public void success(PartnerWrapper partnerWrapper, Response response) {
                            ThanksRegisterDialogFragment dialogFragment = new ThanksRegisterDialogFragment();
                            dialogFragment.show(getActivity().getSupportFragmentManager().beginTransaction(), "thanks");
                            Utils.dismissDialog(PartnerRegisterDialog.this);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
    }

    private boolean checkFields() {
        name.setError(null);
        inn.setError(null);
        phone.setError(null);
        mail.setError(null);
        if (TextUtils.isEmpty(name.getText())) {
            name.setError("Введите название организации");
            return false;
        }
        if (TextUtils.isEmpty(inn.getText())) {
            inn.setError("Введите ИНН");
            return false;
        }
        if (inn.getText().length() < 10) {
            inn.setError("ИНН должен быть 10 символов");
            return false;
        }
        if (TextUtils.isEmpty(phone.getText())) {
            phone.setError("Введите телефон");
            return false;
        }
        if (TextUtils.isEmpty(mail.getText())) {
            mail.setError("Введите e-mail");
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(mail.getText()).matches()) {
            mail.setError("Неверный формат e-mail");
            return false;
        }
        return true;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }
}
