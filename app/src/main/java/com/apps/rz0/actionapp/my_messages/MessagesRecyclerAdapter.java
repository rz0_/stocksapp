package com.apps.rz0.actionapp.my_messages;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.BitmapAjaxCallback;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.model.MessageItem;
import com.apps.rz0.actionapp.comments.CommentActivity;
import com.apps.rz0.actionapp.utils.StaticStrings;

import java.util.ArrayList;


public class MessagesRecyclerAdapter extends RecyclerView.Adapter {
    ArrayList<MessageItem> items;
    private final int MY_MESSAGE_TYPE = 0;
    private final int COMMON_MESSAGE_TYPE = 1;
    AQuery aq;

    public MessagesRecyclerAdapter(ArrayList<MessageItem> items) {
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MessageViewHolder holder;
        View view;
        if (viewType == MY_MESSAGE_TYPE)
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_my_item, parent, false);
        else
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_common_item, parent, false);

        aq = new AQuery(view);
        holder = new MessageViewHolder(view);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), CommentActivity.class);
                intent.putExtra(CommentActivity.ID_MESSAGE, (Integer) v.getTag());
                v.getContext().startActivity(intent);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        MessageViewHolder holder = (MessageViewHolder) viewHolder;
        MessageItem item = items.get(position);

        BitmapAjaxCallback cb = new BitmapAjaxCallback();
        cb.url(StaticStrings.SERVER_URL_2 + item.avatar).rotate(true);
        aq.id(holder.avatar).image(cb);

        holder.message.setText(item.comment);
        holder.time.setText(item.time);
        holder.itemView.setTag(item.messageId);
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position).isUserComment)
            return MY_MESSAGE_TYPE;
        else
            return COMMON_MESSAGE_TYPE;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class MessageViewHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        TextView message;
        TextView time;

        public MessageViewHolder(View itemView) {
            super(itemView);
            avatar = (ImageView) itemView.findViewById(R.id.message_avatar);
            message = (TextView) itemView.findViewById(R.id.text_message);
            time = (TextView) itemView.findViewById(R.id.message_time);
        }
    }
}
