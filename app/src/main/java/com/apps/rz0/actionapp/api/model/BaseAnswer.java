package com.apps.rz0.actionapp.api.model;


public class BaseAnswer<T> {
    private Boolean success;
    private T result;

    public Boolean getSuccess() {
        return success;
    }

    public T getResult() {
        return result;
    }
}
