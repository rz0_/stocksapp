package com.apps.rz0.actionapp.api;

import android.content.Context;
import android.util.Log;

import com.apps.rz0.actionapp.api.model.ServerArrayError;
import com.apps.rz0.actionapp.api.model.ServerError;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;

public class CustomErrorHandler implements ErrorHandler {

    public CustomErrorHandler() {
    }

    @Override
    public Throwable handleError(RetrofitError cause) {
        String errorDescription;

        if (cause.isNetworkError()) {
            errorDescription = "Ошибка сети";
        } else {
            if (cause.getResponse() == null) {
                errorDescription = "Нет ответа от сервера";
            } else {

                // Error message handling - return a simple error to Retrofit handlers..
                try {
                    ServerError errorResponse = (ServerError) cause.getBodyAs(ServerError.class);
                    errorDescription = errorResponse.getErrorMessage();
                } catch (Exception ex) {
                    try {
                        ServerArrayError errorResponse = (ServerArrayError) cause.getBodyAs(ServerArrayError.class);
                        errorDescription = errorResponse.getMessage();
                    } catch (Exception ex2) {
                        Log.e("Server error ", "handleError: " + ex2.getLocalizedMessage());
                        errorDescription = "Неизвестная ошибка, попробуйте позже";
                    }
                }
            }
        }

        return new Exception(errorDescription);
    }
}
