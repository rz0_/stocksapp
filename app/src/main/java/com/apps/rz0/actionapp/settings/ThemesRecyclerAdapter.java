package com.apps.rz0.actionapp.settings;

import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.utils.ThemesUtils;

public class ThemesRecyclerAdapter extends RecyclerView.Adapter {
    String[] colors = new String[]{
            "3db5c6",
            "61bc31",
            "ffd200",
            "f44942",
            ThemesUtils.DEFAULT_COLOR,
            "000000"
    };

    ActionBarActivity activity;

    public ThemesRecyclerAdapter(ActionBarActivity activity) {
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ThemeViewHolder holder = new ThemeViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.theme_item_layout, parent, false));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThemesUtils.saveColorTheme(activity, (String) v.getTag());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ThemeViewHolder holder = (ThemeViewHolder) viewHolder;
        holder.color.setBackgroundColor(Color.parseColor("#" + colors[position]));
        holder.itemView.setTag(colors[position]);
    }

    @Override
    public int getItemCount() {
        return colors.length;
    }

    class ThemeViewHolder extends RecyclerView.ViewHolder {
        ImageView color;

        public ThemeViewHolder(View itemView) {
            super(itemView);
            color = (ImageView) itemView.findViewById(R.id.color_theme);
        }
    }
}
