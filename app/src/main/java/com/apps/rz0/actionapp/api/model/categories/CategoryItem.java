package com.apps.rz0.actionapp.api.model.categories;

import com.apps.rz0.actionapp.categories.ChildCategoryItem;

import java.util.ArrayList;

/**
 * Класс категории
 */
public class CategoryItem {
    private int id;
    private String title;
    private boolean isSelected = false;
    private ArrayList<ChildCategoryItem> childs = new ArrayList<ChildCategoryItem>();

    public boolean getGroupIsSelected() {
        return isSelected;
    }

    public void setGroupSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public int getIdCategory() {
        return id;
    }

    public String getTitleCategory() {
        return title;
    }

    public ArrayList<ChildCategoryItem> getChildCategory() {
        return childs;
    }
}
