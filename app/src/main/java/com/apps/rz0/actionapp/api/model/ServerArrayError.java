package com.apps.rz0.actionapp.api.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ServerArrayError {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("message")
    private List<String> message = new ArrayList<>();

    public String getMessage() {
        String error = "";
        for (String item : message)
            error = error + item + ",\n";
        if (!TextUtils.isEmpty(error))
            return error.substring(0, error.length() - 2);
        else return error;
    }
}
