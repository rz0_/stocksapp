package com.apps.rz0.actionapp.api.model;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("client_id")
    private int idClient;
    @SerializedName("first_name")
    private String name;
    @SerializedName("last_name")
    private String surname;
    private String avatar;
    private String avatarFile;
    private String city;
    @SerializedName("city_id")
    private int idCity;

    public int getIdCity() {
        return idCity;
    }

    public User setIdCity(int idCity) {
        this.idCity = idCity;
        return this;
    }

    public int getIdClient() {
        return idClient;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getAvatarFile() {
        return avatarFile;
    }

    public User setIdClient(int idClient) {
        this.idClient = idClient;
        return this;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public User setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public User setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }

    public User setAvatarFile(String avatarFile) {
        this.avatarFile = avatarFile;
        return this;
    }

    public String getCity() {
        return city;
    }

    public User setCity(String city) {
        this.city = city;
        return this;
    }
}
