package com.apps.rz0.actionapp.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Александр on 25.08.2015.
 */
public class CommentItem {

    @SerializedName("client_id")
    private int idClient;
    private String comment;
    @SerializedName("comment_date")
    private String commentDate;
    @SerializedName("is_partner")
    private boolean isParthner;
    private String image;

    public String getComment() {
        return comment;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public boolean isParthner() {
        return isParthner;
    }

    public String getImage() {
        return image;
    }
}
