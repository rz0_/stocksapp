package com.apps.rz0.actionapp.stocks;

import java.util.ArrayList;

/**
 * Класс обертка для списка акций
 */
public class StocksWrapper {
    private ArrayList<StockItem> stocks = new ArrayList<StockItem>();

    public ArrayList<StockItem> getStocks() {
        return stocks;
    }
}
