package com.apps.rz0.actionapp.registration;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.model.register.LegalType;

import java.util.List;

public class LegalTypesArrayAdapter extends ArrayAdapter<LegalType> {
    public LegalTypesArrayAdapter(Context context, int resource, List<LegalType> objects) {
        super(context, resource, objects);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).legalTypeId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LegalViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.getContext())
                    .inflate(R.layout.spinner_item_layout, parent, false);

            viewHolder = new LegalViewHolder();
            viewHolder.legalName = (TextView) convertView.findViewById(R.id.item_name);

            convertView.setTag(viewHolder);
            convertView.setTag(R.id.tag_id, getItem(position).legalTypeId);
        } else {
            viewHolder = (LegalViewHolder) convertView.getTag();
        }

        viewHolder.legalName.setText(getItem(position).title);

        return convertView;
    }

    class LegalViewHolder {
        TextView legalName;
    }
}
