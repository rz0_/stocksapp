package com.apps.rz0.actionapp.comments;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.model.CommentItem;
import com.apps.rz0.actionapp.utils.StaticStrings;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Александр on 25.08.2015.
 */
public class CommentRecyclerAdapter extends RecyclerView.Adapter {
    private final int MY_TYPE = 0;
    private final int ALL_TYPE = 1;
    ArrayList<CommentItem> items;

    public CommentRecyclerAdapter(ArrayList<CommentItem> items) {
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.common_comment_layout, parent, false);
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        CommentViewHolder holder = (CommentViewHolder) viewHolder;
        CommentItem item = items.get(position);

        holder.message.setText(item.getComment());
        holder.time.setText(item.getCommentDate().substring(item.getCommentDate().indexOf(" "), item.getCommentDate().length()).trim());
        //TODO разрулить правильность даты
        if(!TextUtils.isEmpty(item.getImage())) {
            Picasso.with(viewHolder.itemView.getContext())
                    .load(item.getImage())
                    .error(R.drawable.ic_cap_menu)
                    .into(holder.avatar);
        }
        else
            holder.avatar.setImageResource(R.drawable.ic_cap_menu);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class CommentViewHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        TextView message;
        TextView time;

        public CommentViewHolder(View itemView) {
            super(itemView);
            avatar = (ImageView) itemView.findViewById(R.id.comment_avatar);
            message = (TextView) itemView.findViewById(R.id.comment_message);
            time = (TextView) itemView.findViewById(R.id.comment_time);
        }
    }
}
