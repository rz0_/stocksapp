package com.apps.rz0.actionapp.registration;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.BitmapAjaxCallback;
import com.apps.rz0.actionapp.MainActivity;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.StocksApplication;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.CallbackApi2;
import com.apps.rz0.actionapp.api.model.SaveCityResponce;
import com.apps.rz0.actionapp.api.model.SignedUser;
import com.apps.rz0.actionapp.api.model.User;
import com.apps.rz0.actionapp.choose_dialog.CityItem;
import com.apps.rz0.actionapp.utils.ImagePicker;
import com.apps.rz0.actionapp.utils.StaticStrings;
import com.apps.rz0.actionapp.utils.ThemesUtils;
import com.apps.rz0.actionapp.utils.Utils;

import java.util.ArrayList;

public class PersonalDataDialogFragment extends DialogFragment implements ChooseSocialDialog.OnSocialSetUser {
    public static final String TAG = "personal_data_tag";
    public static final int GALLERY_CODE = 100;

    private AQuery aq;
    private EditText name;
    private EditText surname;
    private Spinner citiesSpinner;
    private String avatarUrl = "";
    private String picturePath;
    private ArrayList<CityItem> cities;
    private int userCityId = 1;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.personal_data_dialog, container, false);
        aq = new AQuery(view);
        aq.id(R.id.toolbar_view_title).text("Личные данные");
        aq.id(R.id.next_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        aq.id(R.id.input_partner_textview).getTextView().setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        name = aq.id(R.id.name_user).getEditText();
        surname = aq.id(R.id.surname_user).getEditText();
        citiesSpinner = aq.id(R.id.cities_spinner).getSpinner();

        if (getArguments().getBoolean(MainActivity.IS_EDIT_PROFILE, false)) {
            aq.id(R.id.next_button).text("Сохранить");
            aq.id(R.id.progress).visible();
            aq.id(R.id.personal_layout).gone();

            Api.get.getClientInfo(Utils.getRegPushId(getActivity()), new Api.OnDataGet<User>() {
                @Override
                public void getData(User user) {
                    aq.id(R.id.progress).gone();
                    if (user == null)
                        return;
                    aq.id(R.id.personal_layout).visible();
                    avatarUrl = StaticStrings.SERVER_URL_2 + user.getAvatar();
                    name.setText(user.getName());
                    surname.setText(user.getSurname());
                    aq.id(R.id.choose_photo_description).gone();
                    aq.id(R.id.login_partner_button).gone();
                    BitmapAjaxCallback cb = new BitmapAjaxCallback();
                    cb.url(avatarUrl).rotate(true);
                    aq.id(R.id.choose_avatar_button).image(cb);
                    if (cities != null && citiesSpinner.getAdapter() != null) {
                        setCityAdapter(user.getIdCity());
                    } else {
                        userCityId = user.getIdCity();
                    }
                }
            });
        } else {
            aq.id(R.id.progress).gone();
            ChooseSocialDialog dialog = new ChooseSocialDialog();
            dialog.show(getChildFragmentManager(), ChooseSocialDialog.TAG);
        }

        Api.get.getAllCities(new Api.OnDataGet<ArrayList<CityItem>>() {
            @Override
            public void getData(ArrayList<CityItem> success) {
                if (success == null || success.size() == 0)
                    return;
                cities = success;

                citiesSpinner.setAdapter(
                        new CityArrayAdapter(getActivity(), R.layout.spinner_main_layout, success));
                if (getArguments().getBoolean(MainActivity.IS_EDIT_PROFILE, false))
                    setCityAdapter(userCityId);
            }
        });

        aq.id(R.id.choose_photo_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = MediaStore.Images.Media.INTERNAL_CONTENT_URI;
                Intent intent = new Intent(Intent.ACTION_PICK, uri);
                intent.setType("image/*");
                startActivityForResult(intent, GALLERY_CODE);
            }
        });

        aq.id(R.id.next_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUserData();
            }
        });

        aq.id(R.id.arrow_home).visible().clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        ThemesUtils.setToolbarColor(getActivity(), aq.id(R.id.toolbar_view).getView());

        return view;
    }

    private void setCityAdapter(int userCityId) {
        for (int i = 0; i < cities.size(); i++) {
            CityItem item = cities.get(i);
            if (item.getCityId() == userCityId) {
                citiesSpinner.setSelection(i);
                break;
            }
        }
    }

    private void saveUserData() {
        if (TextUtils.isEmpty(name.getText())) {
            name.setError("Введите имя");
            return;
        }
        if (TextUtils.isEmpty(surname.getText())) {
            surname.setError("Введите фамилию");
            return;
        }
        String city = ((CityItem) citiesSpinner.getSelectedItem()).getCityTitle();
        final User user = new User()
                .setName(name.getText().toString())
                .setSurname(surname.getText().toString())
                .setCity(city)
                .setIdCity((int) citiesSpinner.getSelectedItemId())
                .setAvatar(avatarUrl)
                .setAvatarFile(picturePath);
        aq.id(R.id.progress).visible();
        StocksApplication.registerApi.fillClient(user, city, new CallbackApi2<SaveCityResponce>() {
            @Override
            public void onSuccess(SaveCityResponce object) {
                Utils.clearUsers(getActivity());
                Utils.saveUser(getActivity(), object.getClient());
                aq.id(R.id.progress).gone();
                dismiss();
            }

            @Override
            public void onError(String error) {
                aq.id(R.id.progress).gone();
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GALLERY_CODE && data != null) {
            loadAvatar(data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loadAvatar(Intent data) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        picturePath = cursor.getString(columnIndex);
        cursor.close();
        BitmapAjaxCallback cb = new BitmapAjaxCallback();
        cb.bitmap(ImagePicker.getImageResized(getActivity(), selectedImage)).rotate(true);
        aq.id(R.id.choose_avatar_button).image(cb);
        aq.id(R.id.choose_photo_description).gone();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void setSocialUser(SignedUser user) {
        BitmapAjaxCallback cb = new BitmapAjaxCallback();
        cb.url(user.getAvatar()).rotate(true);
        aq.id(R.id.choose_avatar_button).image(cb);
        avatarUrl = user.getAvatar();
        aq.id(R.id.choose_photo_description).gone();
        name.setText("");
        name.setText(user.getName());
        surname.setText("");
        surname.setText(user.getSuname());
    }

    @Override
    public void dismiss() {
        super.dismiss();
        getActivity().sendBroadcast(new Intent(StaticStrings.UPDATE_DRAWER_RECEIVER));
    }
}
