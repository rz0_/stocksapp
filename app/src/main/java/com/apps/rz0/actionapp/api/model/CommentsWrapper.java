package com.apps.rz0.actionapp.api.model;

import java.util.ArrayList;

/**
 * Created by Александр on 25.08.2015.
 */
public class CommentsWrapper {
    private ArrayList<CommentItem> comments;

    public ArrayList<CommentItem> getComments() {
        return comments;
    }
}
