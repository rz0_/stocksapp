package com.apps.rz0.actionapp.choose_dialog;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.apps.rz0.actionapp.R;

import java.util.ArrayList;

/**
 * Адаптер для вывода списка городов в диалоге выбора текущего города
 */
public class CityListAdapter extends BaseAdapter {

    ArrayList<CityItem> cityItemsArray = new ArrayList<>();
    Activity context;

    public CityListAdapter(Activity context, ArrayList<CityItem> cityItemsArray) {
        this.context = context;
        this.cityItemsArray = cityItemsArray;
    }

    @Override
    public int getCount() {
        return cityItemsArray.size();
    }

    @Override
    public CityItem getItem(int position) {
        return cityItemsArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getCityId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CityViewHolder holder;
        if (convertView == null) {
            convertView = (context).getLayoutInflater().inflate(R.layout.choose_city_list_item, parent, false);
            holder = new CityViewHolder(convertView);
            convertView.setTag(R.id.tag_holder, holder);
        }

        holder = (CityViewHolder) convertView.getTag(R.id.tag_holder);
        holder.cityName.setText(getItem(position).getCityTitle());
        holder.cityName.setTag(R.id.tag_name, getItem(position).getCityTitle());
        return convertView;
    }

    private class CityViewHolder {
        TextView cityName;

        private CityViewHolder(View view) {
            cityName = (TextView) view.findViewById(R.id.cityNameTextView);
        }
    }
}
