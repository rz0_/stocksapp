package com.apps.rz0.actionapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.apps.rz0.actionapp.api.model.User;
import com.apps.rz0.actionapp.company.CompanyInfoActivity;
import com.apps.rz0.actionapp.my_messages.MessagesActivity;
import com.apps.rz0.actionapp.my_messages.MessagesFragment;
import com.apps.rz0.actionapp.notifications.NotificationHelper;
import com.apps.rz0.actionapp.registration.LoginDialog;
import com.apps.rz0.actionapp.registration.PersonalDataDialogFragment;
import com.apps.rz0.actionapp.settings.SettingsFragment;
import com.apps.rz0.actionapp.shops.ShopsActivity;
import com.apps.rz0.actionapp.social.SocialSDK;
import com.apps.rz0.actionapp.stocks.FavoritesFragment;
import com.apps.rz0.actionapp.stocks.StocksFragment;
import com.apps.rz0.actionapp.stocks.add_stock.AddStockActivity;
import com.apps.rz0.actionapp.utils.StaticStrings;
import com.apps.rz0.actionapp.utils.ThemesUtils;
import com.apps.rz0.actionapp.utils.Utils;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity implements DialogInterface.OnDismissListener {

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    public ListView drawerList;
    Fragment currentFragment;
    int currentTab = 2;
    public StocksFragment stocksFragment;
    private static final String PROFILE_MENU = "Мой профиль";
    public static final String STOCKS_MENU = "Акции";
    private static final String MY_STOCKS_MENU = "Мои отделы";
    private static final String FAVORITES_MENU = "Избранное";
    private static final String MESSAGES_MENU = "Мои сообщения";
    private static final String SETTINGS_MENU = "Настройки";
    private static final String HELP_MENU = "Помощь";
    public static final String ADD_STOCK = "Добавить акцию";
    private static final String CHANGE_PROFILE_MENU = "Сменить учетную запись";

    public static final String IS_EDIT_PROFILE = "edit_profile";

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (drawerAdapter != null) {
                createDrawerMenu();
                drawerAdapter.notifyDataSetChanged();
                setFragment(stocksFragment, 2, STOCKS_MENU);
            }
        }
    };
    private CustomDrawerAdapter drawerAdapter;
    private SettingsFragment settingsFragment;
    private FavoritesFragment favoritesFragment;
    private MessagesFragment messagesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SocialSDK.onCreate(this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        ThemesUtils.setToolbarColor(this);
        NotificationHelper.register(this);
        if (Utils.getRegUser(this) == null && Utils.getPartner(this) == null) {
            LoginDialog loginDialog = new LoginDialog();
            loginDialog.setCancelable(false);
            Bundle args = new Bundle();
            args.putBoolean(LoginDialog.START_FLAG, true);
            loginDialog.setArguments(args);
            loginDialog.show(getSupportFragmentManager().beginTransaction(), "register");
        }
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList = (ListView) findViewById(R.id.left_draw);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, android.R.color.transparent, R.drawable.ic_menu_icon);
        drawerLayout.setDrawerListener(drawerToggle);
        createDrawerMenu();

        stocksFragment = new StocksFragment();
        settingsFragment = new SettingsFragment();
        favoritesFragment = new FavoritesFragment();
        messagesFragment = new MessagesFragment();

        currentFragment = stocksFragment;

        getSupportFragmentManager().beginTransaction().
                detach(currentFragment).
                replace(R.id.main_tabhost, currentFragment).
                attach(currentFragment).
                commit();

        getSupportActionBar().setTitle(STOCKS_MENU);

        drawerList.setItemChecked(currentTab, true);

        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    return;
                }
                String text = ((TextView) view.findViewById(R.id.itemTitle)).getText().toString();

                switch (text) {
                    case STOCKS_MENU: {
                        setFragment(stocksFragment, position, STOCKS_MENU);
                        break;
                    }
                    case PROFILE_MENU: {
                        User user = Utils.getRegUser(MainActivity.this);
                        Bundle args = new Bundle();
                        args.putBoolean(IS_EDIT_PROFILE, true);
                        if (user != null) {
                            PersonalDataDialogFragment dialogFragment = new PersonalDataDialogFragment();
                            dialogFragment.setArguments(args);
                            dialogFragment.show(getSupportFragmentManager(), PersonalDataDialogFragment.TAG);
                        } else {
                            Intent intent = new Intent(MainActivity.this, CompanyInfoActivity.class);
                            intent.putExtra(CompanyInfoActivity.ID_PARTNER, Utils.getPartnerId(MainActivity.this));
                            startActivity(intent);
                        }
                        drawerLayout.closeDrawer(drawerList);
                        break;
                    }
                    case MY_STOCKS_MENU: {
                        drawerLayout.closeDrawer(drawerList);
                        startActivity(new Intent(MainActivity.this, ShopsActivity.class));
                        break;
                    }
                    case MESSAGES_MENU: {
                        setFragment(messagesFragment, position, MESSAGES_MENU);
                        break;
                    }
                    case FAVORITES_MENU: {
                        setFragment(favoritesFragment, position, FAVORITES_MENU);
                        break;
                    }
                    case SETTINGS_MENU: {
                        setFragment(settingsFragment, position, SETTINGS_MENU);
                        break;
                    }
                    case HELP_MENU: {
//                        setFragment(aboutFragment, position);
                        break;
                    }
                    case ADD_STOCK: {
                        drawerLayout.closeDrawer(drawerList);
                        Intent intent = new Intent(MainActivity.this, AddStockActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case CHANGE_PROFILE_MENU: {
                        LoginDialog loginDialog = new LoginDialog();
                        Bundle args = new Bundle();
                        args.putBoolean(LoginDialog.START_FLAG, false);
                        loginDialog.setArguments(args);
                        loginDialog.setCancelable(false);
                        loginDialog.show(getSupportFragmentManager().beginTransaction(), "register");
                        drawerLayout.closeDrawer(drawerList);
                        break;
                    }
                    default: {
                        drawerLayout.closeDrawer(drawerList);
                    }
                }
            }
        });

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_icon);
    }

    private void createDrawerMenu() {
        ArrayList<DrawerItem> drawerItems = new ArrayList<>();
        drawerItems.add(new DrawerItem("", 0));
        drawerItems.add(new DrawerItem(PROFILE_MENU, R.drawable.ic_user_menu));
        drawerItems.add(new DrawerItem(STOCKS_MENU, R.drawable.ic_stocks_menu));
        if (Utils.getPartner(this) != null)
            drawerItems.add(new DrawerItem(MY_STOCKS_MENU, R.drawable.ic_my_stocks));
        drawerItems.add(new DrawerItem(FAVORITES_MENU, R.drawable.ic_my_stocks));
        drawerItems.add(new DrawerItem(MESSAGES_MENU, R.drawable.ic_messages_menu));
        drawerItems.add(new DrawerItem(SETTINGS_MENU, R.drawable.ic_settings_menu));
        drawerItems.add(new DrawerItem(HELP_MENU, R.drawable.ic_help_menu));
        if (Utils.getPartner(this) != null)
            drawerItems.add(new DrawerItem(ADD_STOCK, R.drawable.ic_add_stock));
        drawerItems.add(new DrawerItem(CHANGE_PROFILE_MENU, R.drawable.ic_change_profile_menu, false));

        drawerAdapter = new CustomDrawerAdapter(this, R.layout.drawer_item_layout, drawerItems);
        drawerList.setAdapter(drawerAdapter);
    }

    public void setFragment(Fragment fragment, int position, String title) {
        if (currentFragment == fragment) {
            drawerLayout.closeDrawer(drawerList);
            return;
        }
        getSupportFragmentManager().beginTransaction().
                detach(currentFragment).
                replace(R.id.main_tabhost, fragment).
                attach(fragment).
                commit();

        getSupportActionBar().setTitle(title);
        currentTab = position;
        currentFragment = fragment;
        drawerLayout.closeDrawer(drawerList);
        drawerList.setItemChecked(currentTab, true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        getSupportFragmentManager().beginTransaction().
                detach(currentFragment).
                replace(R.id.main_tabhost, currentFragment).
                attach(currentFragment).
                commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SocialSDK.onResume(this);
        registerReceiver(receiver, new IntentFilter(StaticStrings.UPDATE_DRAWER_RECEIVER));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SocialSDK.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SocialSDK.onActivityResult(MainActivity.this, requestCode, resultCode, data);
    }
}
