package com.apps.rz0.actionapp.categories;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.View;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.StocksApplication;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.CallbackApi;
import com.apps.rz0.actionapp.api.model.categories.CategoriesWrapper;
import com.apps.rz0.actionapp.api.model.categories.CategoryItem;
import com.apps.rz0.actionapp.utils.StaticStrings;
import com.apps.rz0.actionapp.utils.ThemesUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class CategoriesActivity extends ActionBarActivity {
    AQuery aq;
    private CategoriesExpandAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        aq = new AQuery(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.categories_title));
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_button);
        ThemesUtils.setToolbarColor(this);

        getCategories();

        aq.id(R.id.save_categories_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveCategories();
            }
        });
    }

    private void getCategories() {
        Api.get.getCategories(new Api.OnDataGet<ArrayList<CategoryItem>>() {
            @Override
            public void getData(ArrayList<CategoryItem> success) {
                aq.id(R.id.progressBar).gone();
                adapter = new CategoriesExpandAdapter(CategoriesActivity.this, getSelectedCategories(success), success);
                aq.id(R.id.categoriesExpandListView).getExpandableListView().setAdapter(adapter);
            }
        });
    }


    @NonNull
    private SparseArray<ArrayList<ChildCategoryItem>> getSelectedCategories(ArrayList<CategoryItem> categories) {
        SharedPreferences sharedPreferences = getSharedPreferences(StaticStrings.CURRENT_SHARED_PREF, Context.MODE_PRIVATE);
        Gson gson = new Gson();

        final ArrayList<Integer> selectedCategories = gson.fromJson(sharedPreferences.getString(StaticStrings.SELECTED_ACTION, "[]"), new TypeToken<List<Integer>>() {
        }.getType());
        final ArrayList<Integer> unSelectedCategories = gson.fromJson(sharedPreferences.getString(StaticStrings.UNSELECTED_ACTION, "[]"), new TypeToken<List<Integer>>() {
        }.getType());

        SparseArray<ArrayList<ChildCategoryItem>> sparseArray = new SparseArray<>();
        for (CategoryItem item : categories) {
            if (selectedCategories.contains(item.getIdCategory()) || !unSelectedCategories.contains(item.getIdCategory()))
                item.setGroupSelected(true);
            for (ChildCategoryItem childItem : item.getChildCategory())
                if (selectedCategories.contains(childItem.getChildCategoryId()) || !unSelectedCategories.contains(childItem.getChildCategoryId()))
                    childItem.setChildSelected(true);
            sparseArray.append(item.getIdCategory(), item.getChildCategory());
        }
        return sparseArray;
    }

    private void saveCategories() {
        SharedPreferences sharedPreferences = getSharedPreferences(StaticStrings.CURRENT_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        ArrayList<Integer> selectedIds = new ArrayList<>();
        ArrayList<Integer> unselectedIds = new ArrayList<>();

        if (adapter != null) {
            ArrayList<CategoryItem> array = adapter.categoriesArrayList;
            for (CategoryItem item : array) {
                if (item.getGroupIsSelected()) {
                    selectedIds.add(item.getIdCategory());
                } else {
                    unselectedIds.add(item.getIdCategory());
                    for (ChildCategoryItem childItem : item.getChildCategory()) {
                        if (childItem.getChildIsSelected()) {
                            selectedIds.add(childItem.getChildCategoryId());
                        } else {
                            unselectedIds.add(childItem.getChildCategoryId());
                        }
                    }
                }
            }
        }

        Gson gson = new Gson();
        String json = gson.toJson(selectedIds);
        String json_un = gson.toJson(unselectedIds);
        editor.putString(StaticStrings.SELECTED_ACTION, json);
        editor.putString(StaticStrings.UNSELECTED_ACTION, json_un);
        StocksApplication.serverController.saveCategories(selectedIds, new CallbackApi() {
            @Override
            public void onSuccess(String json) {
                finish();
            }

            @Override
            public void onError(String error) {
                finish();
            }
        });
        editor.apply();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
