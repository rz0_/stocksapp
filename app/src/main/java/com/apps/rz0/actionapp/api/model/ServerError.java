package com.apps.rz0.actionapp.api.model;

import com.google.gson.annotations.SerializedName;

public class ServerError {
    boolean success;
    @SerializedName("message")
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }
}
