package com.apps.rz0.actionapp.registration;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.MainActivity;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.StocksApplication;
import com.apps.rz0.actionapp.api.CallbackApi2;
import com.apps.rz0.actionapp.api.model.RegisterId;
import com.apps.rz0.actionapp.utils.StaticStrings;
import com.apps.rz0.actionapp.utils.Utils;

public class LoginDialog extends DialogFragment {
    private AQuery aq;
    boolean flagBack = true;
    private EditText phoneEditText;
    public static String START_FLAG = "start_flag";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_dialog, container, false);
        aq = new AQuery(view);

        aq.id(R.id.login_partner_button).getTextView().setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        aq.id(R.id.progress).gone();
        phoneEditText = aq.id(R.id.phone_edittext).getEditText();
        phoneEditText.addTextChangedListener(new MaskedWatcher(phoneEditText, "+7(###) ###-##-##"));

        aq.id(R.id.next_button_register).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagBack)
                    clickRegister();
                else
                    clickActivate();
            }
        });
        aq.id(R.id.login_partner_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PartnerLoginDialog partnerLoginDialog = new PartnerLoginDialog();
                partnerLoginDialog.show(getActivity().getSupportFragmentManager().beginTransaction(), PartnerLoginDialog.TAG);
                partnerLoginDialog.setCancelable(false);
                Utils.dismissDialog(LoginDialog.this);

            }
        });
        return view;
    }


    private void clickActivate() {
        EditText codeEditText = aq.id(R.id.code_edittext).getEditText();
        if (TextUtils.isEmpty(codeEditText.getText())) {
            codeEditText.setError("Код не может быть пустым");
            return;
        }
        aq.id(R.id.progress).visible();
        StocksApplication.registerApi.activateUser(phoneEditText.getText().toString(), codeEditText.getText().toString(),
                new CallbackApi2<RegisterId>() {
                    @Override
                    public void onSuccess(RegisterId object) {
                        aq.id(R.id.progress).gone();
                        PersonalDataDialogFragment dialog = new PersonalDataDialogFragment();
                        if (getArguments().getBoolean(START_FLAG, false))
                            dialog.setCancelable(false);
                        Bundle args = new Bundle();
                        args.putBoolean(MainActivity.IS_EDIT_PROFILE, false);
                        dialog.setArguments(args);
                        dialog.show(getActivity().getSupportFragmentManager(), PersonalDataDialogFragment.TAG);
                        Utils.dismissDialog(LoginDialog.this);
                    }

                    @Override
                    public void onError(String error) {
                        aq.id(R.id.progress).gone();
//                        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void clickRegister() {
        phoneEditText.setError(null);
        String phone = phoneEditText.getText().toString();
        phone = phone.replaceAll("\\s", "");
        phone = phone.replaceAll("-", "");
        if (phone.length() < 11) {
            phoneEditText.setError("Введите правильный номер");
            return;
        }
        aq.id(R.id.progress).visible();
        StocksApplication.registerApi.register(phone, new CallbackApi2<RegisterId>() {
            @Override
            public void onSuccess(RegisterId item) {
                aq.id(R.id.progress).gone();
                if (TextUtils.isEmpty(item.client.getName()) &&
                        TextUtils.isEmpty(item.client.getSurname()) &&
                        TextUtils.isEmpty(item.client.getCity())) {
                    aq.id(R.id.code_edittext).visible();
                    phoneEditText.setEnabled(false);
                    aq.id(R.id.description_register).gone();
                    flagBack = false;
                } else {
                    Utils.clearUsers(getActivity());
                    Utils.saveUser(getActivity(), item.client);
                    getActivity().sendBroadcast(new Intent(StaticStrings.UPDATE_DRAWER_RECEIVER));
                    dismiss();
                }
            }

            @Override
            public void onError(String error) {
                aq.id(R.id.progress).gone();
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(android.content.DialogInterface dialog, int keyCode,
                                 android.view.KeyEvent event) {

                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                    if (flagBack)
                        return false;
                    else {
                        flagBack = true;
                        aq.id(R.id.code_edittext).gone();
                        phoneEditText.setEnabled(true);
                        aq.id(R.id.description_register).visible();
                        return true;
                    }
                } else
                    return false;
            }
        });
    }
}
