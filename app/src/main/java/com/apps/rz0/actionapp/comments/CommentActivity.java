package com.apps.rz0.actionapp.comments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.StocksApplication;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.model.CommentItem;
import com.apps.rz0.actionapp.utils.ThemesUtils;
import com.apps.rz0.actionapp.utils.Utils;

import java.util.ArrayList;

public class CommentActivity extends ActionBarActivity implements SwipeRefreshLayout.OnRefreshListener {
    AQuery aq;
    RecyclerView commentRecyclerView;
    public static final String ID_MESSAGE = "id_message";
    private CommentRecyclerAdapter adapter;
    int idMessage;
    SwipeRefreshLayout swipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        aq = new AQuery(this);
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setEnabled(true);
        swipeLayout.setOnRefreshListener(this);
        commentRecyclerView = (RecyclerView) findViewById(R.id.comment_recyclerview);
        commentRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        setSupportActionBar(toolbar);
        idMessage = getIntent().getIntExtra(ID_MESSAGE, 0);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Комментарии");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_button);
        ThemesUtils.setToolbarColor(this);

        aq.id(R.id.send_comment).clicked(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Api.get.addComment(Utils.getUserId(aq.getContext()), Utils.getPartnerId(v.getContext()), idMessage,
                        aq.id(R.id.text_comment).getText().toString(), new Api.OnDataGet<String>() {
                            @Override
                            public void getData(String success) {
                                getComments();
                                aq.id(R.id.text_comment).text("");
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            }
                        });

            }
        });

        addActionViewed();
        getComments();
    }

    private void addActionViewed() {
        StocksApplication.stockApi.addViewedAction(idMessage);
    }

    private void getComments() {
        Api.get.getComments(idMessage, new Api.OnDataGet<ArrayList<CommentItem>>() {
            @Override
            public void getData(final ArrayList<CommentItem> success) {
                aq.id(R.id.progress).gone();
                swipeLayout.setRefreshing(false);
                if (success == null || success.size() == 0)
                    return;
                if (adapter == null) {
                    adapter = new CommentRecyclerAdapter(success);
                    commentRecyclerView.setAdapter(adapter);
                } else {
                    adapter.items = success;
                    adapter.notifyDataSetChanged();
                }
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        commentRecyclerView.smoothScrollToPosition(success.size() - 1);
                    }
                }, 100);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        swipeLayout.setRefreshing(true);
        getComments();
    }
}
