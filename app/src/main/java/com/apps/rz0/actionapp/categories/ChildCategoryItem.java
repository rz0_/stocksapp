package com.apps.rz0.actionapp.categories;

/**
 * Класс для подкатегории
 */
public class ChildCategoryItem {
    private int id;
    private String title;
    private boolean isSelected = false;

    public boolean getChildIsSelected() {
        return isSelected;
    }

    public void setChildSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public int getChildCategoryId() {
        return id;
    }

    public String getTitleChildCategory() {
        return title;
    }
}
