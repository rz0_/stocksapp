package com.apps.rz0.actionapp.api;

import android.content.Context;
import android.text.TextUtils;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.apps.rz0.actionapp.GsonTransformer;
import com.apps.rz0.actionapp.StocksApplication;
import com.apps.rz0.actionapp.choose_dialog.CitiesListWrapper;
import com.apps.rz0.actionapp.choose_dialog.CityItem;
import com.apps.rz0.actionapp.utils.StaticStrings;
import com.apps.rz0.actionapp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Серверное API
 */
public class ServerAPI implements InterfaceServerAPI {

    Context context;
    AQuery aq;

    public ServerAPI(Context context) {
        this.context = context;
        aq = new AQuery(context);
    }

    /**
     * Метод для получения города по координатам
     *
     * @param lat      широта
     * @param lon      долгота
     * @param callback Callback
     */

    @Override
    public void getCityByCoords(double lat, double lon, final CallbackApi callback) {
        String url = StaticStrings.SERVER_URL + "api/getCityByCoords";

        HashMap<String, Object> params = new HashMap<>();
        params.put("lat", lat);
        params.put("lon", lon);

        aq.ajax(url, params, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (status.getCode() == 200) {
                    callback.onSuccess(object);
                } else {
                    callback.onError(status.getMessage());
                }
            }
        });

    }


    /**
     * Метод для получения списка категорий
     *
     * @param callback Callback
     */
    @Override
    public void getCategoriesList(final CallbackApi callback) {
        String url = StaticStrings.SERVER_URL + "api/getCategories";

        aq.ajax(url, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (status.getCode() == 200) {
                    callback.onSuccess(object);
                } else {
                    callback.onError(status.getMessage());
                }
            }
        });
    }

    /**
     * Получение списка акций
     *
     * @param cityId     ID города
     * @param categories список категорий
     * @param callback   Callback
     */
    @Override
    public void getStocksList(long cityId, String categories, final CallbackApi callback) {
        String url = StaticStrings.SERVER_URL + "api/getStocks";
        HashMap<String, Object> params = new HashMap<>();

        params.put("city_id", cityId);
        if (StocksApplication.currentUser != null)
            params.put("reg_id", Utils.getRegPushId(aq.getContext()));
        if (!TextUtils.isEmpty(categories))
            params.put("categories", categories);

        aq.ajax(url, params, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (status.getCode() == 200) {
                    callback.onSuccess(object);
                } else {
                    callback.onError(status.getMessage());
                }
            }
        });
    }

    /**
     * Привязка города к пользователю
     *
     * @param cityId   ID города
     * @param userId   ID пользователя
     * @param callback Callback
     */
    @Override
    public void saveChoosedCity(long cityId, String userId, final CallbackApi callback) {
        String url = StaticStrings.SERVER_URL + "api/setClientCity";
        HashMap<String, Object> params = new HashMap<>();

        params.put("city_id", cityId);
        params.put("reg_id", userId);

        aq.ajax(url, params, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (status.getCode() == 200) {
                    callback.onSuccess(object);
                } else {
                    callback.onError(status.getMessage());
                }
            }
        });
    }

    /**
     * Сохранение списка категорий
     *
     * @param idsCategories список категорий
     * @param userId        ID пользователя
     * @param callback      Callback
     */
    @Override
    public void saveCategories(String idsCategories, String userId, final CallbackApi callback) {
        String url = StaticStrings.SERVER_URL + "api/setClientCategory";
        HashMap<String, Object> params = new HashMap<>();

        params.put("category_id", idsCategories);
        params.put("reg_id", userId);

        aq.ajax(url, params, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (status.getCode() == 200) {
                    callback.onSuccess(object);
                } else {
                    callback.onError(status.getMessage());
                }
            }
        });
    }

    @Override
    public void savePhone(String phone, String userId, final CallbackApi callback) {
        String url = StaticStrings.SERVER_URL + "api/setClientPhone";
        HashMap<String, Object> params = new HashMap<>();

        params.put("phone", phone);
        params.put("reg_id", userId);

        aq.ajax(url, params, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (status.getCode() == 200) {
                    if (callback != null)
                        callback.onSuccess(object);
                } else {
                    if (callback != null)
                        callback.onError(status.getMessage());
                }
            }
        });
    }

    @Override
    public void savePushStatus(int isPushEnabled, String userId, final CallbackApi callback) {
        String url = StaticStrings.SERVER_URL + "api/setClientPushStatus";
        HashMap<String, Object> params = new HashMap<>();

        params.put("status", isPushEnabled);
        params.put("reg_id", userId);

        aq.ajax(url, params, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (status.getCode() == 200) {
                    if (callback != null)
                        callback.onSuccess(object);
                } else {
                    if (callback != null)
                        callback.onError(status.getMessage());
                }
            }
        });
    }

    @Override
    public void saveSmsStatus(int isSmsEnabled, String userId, final CallbackApi callback) {
        String url = StaticStrings.SERVER_URL + "api/setClientSmsStatus";
        HashMap<String, Object> params = new HashMap<>();

        params.put("status", isSmsEnabled);
        params.put("reg_id", userId);

        aq.ajax(url, params, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (status.getCode() == 200) {
                    if (callback != null)
                        callback.onSuccess(object);
                } else {
                    if (callback != null)
                        callback.onError(status.getMessage());
                }
            }
        });
    }
}
