package com.apps.rz0.actionapp.api.model.register;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LegalWrapper {
    @SerializedName("legal_types")
    public ArrayList<LegalType> legalTypes = new ArrayList<>();
}
