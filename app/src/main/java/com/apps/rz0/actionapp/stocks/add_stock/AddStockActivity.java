package com.apps.rz0.actionapp.stocks.add_stock;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.BitmapAjaxCallback;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.utils.ImagePicker;
import com.apps.rz0.actionapp.utils.ThemesUtils;
import com.apps.rz0.actionapp.utils.Utils;
import com.soundcloud.android.crop.Crop;

import java.io.File;

public class AddStockActivity extends ActionBarActivity {
    public static final int GALLERY_CODE = 100;
    public static final int FILTER_CODE = 200;
    private String picturePath;
    private AQuery aq;
    private EditText name;
    private EditText description;
    private Uri destination;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_stock);
        aq = new AQuery(this);
        name = aq.id(R.id.title_stock).getEditText();
        description = aq.id(R.id.description_stock).getEditText();
        Api.get.updateToken(Utils.getPartnerLogin(this), Utils.getPartnerPass(this));

        aq.id(R.id.choose_picture_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(AddStockActivity.this);
                startActivityForResult(chooseImageIntent, GALLERY_CODE);

            }
        });

        aq.id(R.id.next_button_stock).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkFields()) {
                    Intent intent = new Intent(AddStockActivity.this, AddStockNextActivity.class);
                    intent.putExtra(AddStockNextActivity.SHORT_DESCRIPTION_STOCK, name.getText().toString());
                    intent.putExtra(AddStockNextActivity.DESCRIPTION_STOCK, description.getText().toString());
                    intent.putExtra(AddStockNextActivity.PATH_IMAGE_STOCK, picturePath);
                    startActivity(intent);
                }
            }
        });

        ThemesUtils.setToolbar(this, "Добавить акцию");
    }

    private boolean checkFields() {
        if (TextUtils.isEmpty(picturePath)) {
            Toast.makeText(this, "Выберите валидное изображение", Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(name.getText())) {
            name.setError("Введите название");
            return false;
        }
        if (TextUtils.isEmpty(description.getText())) {
            description.setError("Введите описание");
            return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GALLERY_CODE && data != null) {
            destination = Uri.fromFile(new File(getCacheDir(), "cropped.png"));
            Crop.of(data.getData(), destination).withAspect(4, 3).start(this);
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap = ImagePicker.getImageFromResult(this, Activity.RESULT_OK, destination);
            aq.id(R.id.choosed_image).getImageView().setImageBitmap(bitmap);
            aq.id(R.id.choose_avatar_button).gone();

            Intent intent = new Intent(AddStockActivity.this, FilterActivity.class);
            intent.putExtra(FilterActivity.IMAGE, destination.getPath());
            startActivityForResult(intent, FILTER_CODE);

        } else if (requestCode == FILTER_CODE && resultCode == RESULT_OK) {
            BitmapAjaxCallback cb = new BitmapAjaxCallback();
            cb.url(data.getStringExtra(FilterActivity.IMAGE_URL)).rotate(true);
            picturePath = data.getStringExtra(FilterActivity.IMAGE);
            aq.id(R.id.choosed_image).image(cb);
            aq.id(R.id.choose_avatar_button).gone();
            aq.id(R.id.choose_photo_description).gone();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
