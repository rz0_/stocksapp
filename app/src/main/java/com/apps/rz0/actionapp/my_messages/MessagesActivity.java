package com.apps.rz0.actionapp.my_messages;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.model.MessageItem;
import com.apps.rz0.actionapp.utils.ThemesUtils;
import com.apps.rz0.actionapp.utils.Utils;

import java.util.ArrayList;

public class MessagesActivity extends ActionBarActivity implements SwipeRefreshLayout.OnRefreshListener{
    private AQuery aq;
    private RecyclerView messagesRecyclerView;
    SwipeRefreshLayout swipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        aq = new AQuery(this);
        aq.id(R.id.progress).visible();
        aq.id(R.id.toolbar_view_title).text(getResources().getString(R.string.messages_menu));

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setEnabled(true);
        swipeLayout.setOnRefreshListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.messages_title));
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_button);
        ThemesUtils.setToolbarColor(this);

        messagesRecyclerView = (RecyclerView) findViewById(R.id.messages_recyclerview);
        messagesRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        getMessages();

    }

    private void getMessages() {
        Api.get.getDialogs(Utils.getRegPushId(this), new Api.OnDataGet<ArrayList<MessageItem>>() {
            @Override
            public void getData(ArrayList<MessageItem> success) {
                aq.id(R.id.progress).gone();
                swipeLayout.setRefreshing(false);
                if (success != null && success.size() > 0)
                    messagesRecyclerView.setAdapter(new MessagesRecyclerAdapter(success));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        swipeLayout.setRefreshing(true);
        getMessages();
    }
}
