package com.apps.rz0.actionapp.api.model.stock;

import com.google.gson.annotations.SerializedName;

public class PayItem {
    @SerializedName("message_id")
    public String messageId;
    @SerializedName("pay_link")
    public String payLink;
}
