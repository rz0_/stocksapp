package com.apps.rz0.actionapp.shops;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.model.shops.ShopItem;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ShopsRecyclerAdapter extends RecyclerView.Adapter {
    ArrayList<ShopItem> items;
    ActionBarActivity activity;

    public ShopsRecyclerAdapter(ActionBarActivity activity, ArrayList<ShopItem> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_item, parent, false);
        ShopViewHolder holder = new ShopViewHolder(view);
        holder.pencil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShopItem item = (ShopItem) v.getTag();
                AddShopDialog dialog = new AddShopDialog();
                Bundle args = new Bundle();
                args.putString(AddShopDialog.ITEM, new Gson().toJson(item));
                args.putBoolean(AddShopDialog.IS_EDIT_ITEM, true);
                dialog.setArguments(args);
                dialog.show(activity.getSupportFragmentManager(), AddShopDialog.TAG);
            }
        });
        holder.recycler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer idShop = (Integer) v.getTag();
                showConfirmDialog(idShop);

            }
        });
        return holder;
    }

    private void showConfirmDialog(final int idShop) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View view = activity.getLayoutInflater().inflate(R.layout.confirm_choosed_city_layout, null);

        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();
        view.findViewById(R.id.yesButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Api.get.deleteShop(idShop, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        ((ShopsActivity) activity).updateAdapter();
                        dialog.dismiss();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        dialog.dismiss();
                    }
                });
            }
        });
        view.findViewById(R.id.noButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ShopViewHolder holder = (ShopViewHolder) viewHolder;
        ShopItem item = items.get(position);
        holder.street.setText("ул. " + item.address);
        holder.pencil.setTag(item);
        holder.recycler.setTag(item.idShop);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class ShopViewHolder extends RecyclerView.ViewHolder {
        TextView street;
        ImageView pencil;
        ImageView recycler;

        public ShopViewHolder(View itemView) {
            super(itemView);
            street = (TextView) itemView.findViewById(R.id.street_title);
            pencil = (ImageView) itemView.findViewById(R.id.pencil_icon);
            recycler = (ImageView) itemView.findViewById(R.id.recycler_icon);
        }
    }
}
