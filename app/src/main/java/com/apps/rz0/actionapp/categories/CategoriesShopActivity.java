package com.apps.rz0.actionapp.categories;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.View;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.model.categories.CategoryItem;
import com.apps.rz0.actionapp.utils.ThemesUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class CategoriesShopActivity extends ActionBarActivity {
    public static final String CATEGORIES = "categories";
    AQuery aq;
    private CategoriesExpandAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories_shop);
        aq = new AQuery(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.categories_title));
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_button);
        ThemesUtils.setToolbarColor(this);

        getCategories();

        aq.id(R.id.save_categories_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(CATEGORIES, returnCategories());
                setResult(RESULT_OK, intent);
                finish();

            }
        });
    }

    private void getCategories() {
        Api.get.getCategories(new Api.OnDataGet<ArrayList<CategoryItem>>() {
            @Override
            public void getData(ArrayList<CategoryItem> success) {
                aq.id(R.id.progressBar).gone();
                adapter = new CategoriesExpandAdapter(CategoriesShopActivity.this, getSelectedCategories(success), success);
                aq.id(R.id.categoriesExpandListView).getExpandableListView().setAdapter(adapter);
            }
        });
    }


    @NonNull
    private SparseArray<ArrayList<ChildCategoryItem>> getSelectedCategories(ArrayList<CategoryItem> categories) {

        final ArrayList<Integer> selectedCategories = new Gson().fromJson("[" + getIntent().getStringExtra(CATEGORIES) + "]",
                new TypeToken<List<Integer>>() {
                }.getType());

        SparseArray<ArrayList<ChildCategoryItem>> sparseArray = new SparseArray<>();
        for (CategoryItem item : categories) {
            if (selectedCategories.contains(item.getIdCategory()))
                item.setGroupSelected(true);
            for (ChildCategoryItem childItem : item.getChildCategory())
                if (selectedCategories.contains(childItem.getChildCategoryId()))
                    childItem.setChildSelected(true);
            sparseArray.append(item.getIdCategory(), item.getChildCategory());
        }
        return sparseArray;
    }

    private String returnCategories() {
        ArrayList<Integer> selectedIds = new ArrayList<>();

        if (adapter != null) {
            ArrayList<CategoryItem> array = adapter.categoriesArrayList;
            for (CategoryItem item : array) {
                if (item.getGroupIsSelected()) {
                    selectedIds.add(item.getIdCategory());
                } else {
                    for (ChildCategoryItem childItem : item.getChildCategory()) {
                        if (childItem.getChildIsSelected()) {
                            selectedIds.add(childItem.getChildCategoryId());
                        }
                    }
                }
            }
        }
        return new Gson().toJson(selectedIds);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
