package com.apps.rz0.actionapp.registration;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.CallbackApi2;
import com.apps.rz0.actionapp.api.model.SignedUser;
import com.apps.rz0.actionapp.social.SocialNet;
import com.apps.rz0.actionapp.social.SocialSDK;

/**
 * Created by Александр on 19.08.2015.
 */
public class ChooseSocialDialog extends DialogFragment {
    public static final String TAG = "personal_data_tag";

    public interface OnSocialSetUser {
        void setSocialUser(SignedUser user);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.choose_social_dialog, container, false);
        AQuery aq = new AQuery(view);
        View.OnClickListener socialClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SocialSDK.authorize((ActionBarActivity) getActivity(), (SocialNet) v.getTag(), new CallbackApi2<SignedUser>() {
                    @Override
                    public void onSuccess(SignedUser object) {
                        dismiss();
                        ((OnSocialSetUser) getParentFragment()).setSocialUser(object);
                    }

                    @Override
                    public void onError(String error) {
                        dismiss();
                    }
                });
            }
        };
        aq.id(R.id.vk_button).clicked(socialClickListener).tag(SocialNet.VKONTAKTE);
        aq.id(R.id.fb_button).clicked(socialClickListener).tag(SocialNet.FACEBOOK);
        aq.id(R.id.ok_button).clicked(socialClickListener).tag(SocialNet.ODNOKLASSNIKI);
        aq.id(R.id.manually_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setGravity(Gravity.CENTER);
//        WindowManager.LayoutParams p = dialog.getWindow().getAttributes();
//        p.width = ViewGroup.LayoutParams.MATCH_PARENT;
//        p.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE;
//        p.x = 300;
//        dialog.getWindow().setAttributes(p);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        int width = getActivity().getResources().getDisplayMetrics().widthPixels;
        int height = getActivity().getResources().getDisplayMetrics().heightPixels;
        dialog.getWindow().setLayout(width, height / 2);

        return dialog;
    }
}
