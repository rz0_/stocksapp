package com.apps.rz0.actionapp.company;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.MainActivity;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.model.company.CompanyItem;
import com.apps.rz0.actionapp.registration.PartnerMoreDialog;
import com.apps.rz0.actionapp.utils.ThemesUtils;

public class CompanyInfoActivity extends ActionBarActivity {

    public static final String ID_PARTNER = "id_partner";
    private AQuery aq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        aq = new AQuery(this);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_button);
        getSupportActionBar().setTitle("Информация о партнере");
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.company_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(CompanyInfoActivity.this));
        ThemesUtils.setToolbarColor(this);

        Api.get.getCompanyInfo(getIntent().getIntExtra(ID_PARTNER, -1), new Api.OnDataGet<CompanyItem>() {
            @Override
            public void getData(CompanyItem success) {
                aq.id(R.id.progress).gone();
                if (success == null)
                    return;
                recyclerView.setAdapter(new CompanyInfoAdapter(CompanyInfoActivity.this, success));
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        } else if (id == R.id.action_partner_profile) {
            PartnerMoreDialog dialog = new PartnerMoreDialog();
            Bundle args = new Bundle();
            args.putBoolean(MainActivity.IS_EDIT_PROFILE, true);
            dialog.setArguments(args);
            dialog.show(getSupportFragmentManager(), PartnerMoreDialog.TAG);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_company_info, menu);
        return true;
    }
}
