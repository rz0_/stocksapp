package com.apps.rz0.actionapp.social;

public interface SocialCallback {
    public void onSuccess(String json);
    public void onError(String error);
}
