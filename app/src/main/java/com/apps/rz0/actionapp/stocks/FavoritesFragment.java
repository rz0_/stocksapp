package com.apps.rz0.actionapp.stocks;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.utils.Utils;

import java.util.ArrayList;

public class FavoritesFragment extends Fragment {
    RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        final View progress = view.findViewById(R.id.progress);
        progress.setVisibility(View.VISIBLE);

        recyclerView = (RecyclerView) view.findViewById(R.id.favorites_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        Api.get.getFavoritesStocks(Utils.getRegPushId(getActivity()), new Api.OnDataGet<ArrayList<StockItem>>() {
            @Override
            public void getData(ArrayList<StockItem> success) {
                progress.setVisibility(View.GONE);
                if (success == null || success.size() == 0)
                    return;
                recyclerView.setAdapter(new StocksAdapter((ActionBarActivity) getActivity(), success));
            }
        });
        return view;
    }


}
