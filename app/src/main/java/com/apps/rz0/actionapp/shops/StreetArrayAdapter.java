package com.apps.rz0.actionapp.shops;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.apps.rz0.actionapp.R;

import java.util.List;

public class StreetArrayAdapter extends ArrayAdapter<String> {
    public StreetArrayAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CountryViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.getContext())
                    .inflate(R.layout.spinner_main_layout, parent, false);

            viewHolder = new CountryViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.item_name);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (CountryViewHolder) convertView.getTag();
        }

        viewHolder.name.setText(getItem(position));

        return convertView;
    }

    class CountryViewHolder {
        TextView name;
    }
}
