package com.apps.rz0.actionapp.api.model.shops;

import com.apps.rz0.actionapp.stocks.StockItem;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ShopFullItem {
    @SerializedName("shop_id")
    public int idShop;

    public String title;

    public String city;

    public String address;

    @SerializedName("address_additional")
    public String additional;

    public String description;

    public String phone;

    @SerializedName("stocks_num")
    public String stocksCount;

    @SerializedName("likes_num")
    public String likes;

    @SerializedName("views_num")
    public String views;

    public String photo;
    public Coordinates coords;

    public ArrayList<StockItem> stocks;
}
