package com.apps.rz0.actionapp.my_messages;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.model.MessageItem;
import com.apps.rz0.actionapp.utils.ThemesUtils;
import com.apps.rz0.actionapp.utils.Utils;

import java.util.ArrayList;

public class MessagesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private AQuery aq;
    private RecyclerView messagesRecyclerView;
    private SwipeRefreshLayout swipeLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_messages, container, false);

        aq = new AQuery(view);
        aq.id(R.id.progress).visible();

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeLayout.setEnabled(true);
        swipeLayout.setOnRefreshListener(this);

        messagesRecyclerView = (RecyclerView) view.findViewById(R.id.messages_recyclerview);
        messagesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        getMessages();

        return view;
    }

    private void getMessages() {
        Api.get.getDialogs(Utils.getRegPushId(getActivity()), new Api.OnDataGet<ArrayList<MessageItem>>() {
            @Override
            public void getData(ArrayList<MessageItem> success) {
                aq.id(R.id.progress).gone();
                swipeLayout.setRefreshing(false);
                if (success != null && success.size() > 0)
                    messagesRecyclerView.setAdapter(new MessagesRecyclerAdapter(success));
            }
        });
    }

    @Override
    public void onRefresh() {
        swipeLayout.setRefreshing(true);
        getMessages();
    }
}
