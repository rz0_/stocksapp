package com.apps.rz0.actionapp.shops;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.model.shops.ShopItem;
import com.apps.rz0.actionapp.utils.ThemesUtils;
import com.apps.rz0.actionapp.utils.Utils;

import java.util.ArrayList;

public class ShopsActivity extends ActionBarActivity implements SwipeRefreshLayout.OnRefreshListener{
    private AQuery aq;
    RecyclerView shopsRecyclerView;
    SwipeRefreshLayout swipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shops);
        aq = new AQuery(this);
        aq.id(R.id.progress).visible();
        aq.id(R.id.toolbar_view_title).text(getResources().getString(R.string.shops_title));
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setEnabled(true);
        swipeLayout.setOnRefreshListener(this);
        Api.get.updateToken(Utils.getPartnerLogin(this), Utils.getPartnerPass(this));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.shops_title));
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_button);
        ThemesUtils.setToolbarColor(this);

        shopsRecyclerView = (RecyclerView) findViewById(R.id.shops_recyclerview);
        shopsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        updateAdapter();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void updateAdapter() {
        Api.get.getShops(Utils.getPartnerId(this), new Api.OnDataGet<ArrayList<ShopItem>>() {
            @Override
            public void getData(ArrayList<ShopItem> success) {
                aq.id(R.id.progress).gone();
                swipeLayout.setRefreshing(false);
                if (success != null) {
                    if (shopsRecyclerView.getAdapter() == null)
                        shopsRecyclerView.setAdapter(new ShopsRecyclerAdapter(ShopsActivity.this, success));
                    else {
                        ((ShopsRecyclerAdapter) shopsRecyclerView.getAdapter()).items = success;
                        shopsRecyclerView.getAdapter().notifyDataSetChanged();
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_shops, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        if (id == R.id.action_add_shop) {
            AddShopDialog dialog = new AddShopDialog();
            Bundle args = new Bundle();
            args.putBoolean(AddShopDialog.IS_EDIT_ITEM, false);
            dialog.setArguments(args);
            dialog.setUpdateShopsInterface(new UpdateShopsInterface() {
                @Override
                public void updateShops() {
                    updateAdapter();
                }
            });
            dialog.show(getSupportFragmentManager(), AddShopDialog.TAG);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        swipeLayout.setRefreshing(true);
            updateAdapter();
    }
}
