package com.apps.rz0.actionapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;

import com.apps.rz0.actionapp.StocksApplication;
import com.apps.rz0.actionapp.api.model.ServerError;
import com.apps.rz0.actionapp.api.model.User;
import com.apps.rz0.actionapp.api.model.register.PartnerItem;
import com.apps.rz0.actionapp.notifications.NotificationHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Александр on 28.07.2015.
 */
public class Utils {
    private static final String REGISTER_PREF = "register_pref";
    private static final String REGISTER_ID = "register_id";
    private static final String REGISTER_USER = "register_user";
    private static final String PARTNER = "partner";
    private static final String PARTNER_ID = "partner_id";
    private static final String PARTNER_TOKEN = "partner_token";
    private static final String PARTNER_LOGIN = "partner_login";
    private static final String PARTNER_PASSWORD = "partner_password";

    public static void dismissDialog(final DialogFragment fragment) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    fragment.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1000);
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static String getRegPushId(Context context) {
        return NotificationHelper.getRegistrationId(context);
    }

    public static void saveUser(Context context, User user) {
        SharedPreferences preferences = context.getSharedPreferences(REGISTER_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(REGISTER_ID, user.getIdClient());
        editor.putString(REGISTER_USER, new Gson().toJson(user));

        editor.apply();
        StocksApplication.currentUser = user;
    }

    public static String getSelectedCategories(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(StaticStrings.CURRENT_SHARED_PREF, Context.MODE_PRIVATE);
        final ArrayList<Integer> selectedCategories = new Gson()
                .fromJson(sharedPreferences.getString(StaticStrings.SELECTED_ACTION, "[]"), new TypeToken<List<Integer>>() {
                }.getType());

        String ids = "";
        for (Integer id : selectedCategories)
            ids += String.valueOf(id) + ",";

        if (ids.length() > 0)
            ids = ids.substring(0, ids.length() - 1);
        return ids;
    }

    public static int getCityId(Context context) {
        int idCity = 1;
        if (Utils.getRegUser(context) != null){
            idCity = getRegUser(context).getIdCity();
        }
        if(Utils.getPartner(context) != null) {
            idCity = getPartner(context).cityId;
        }

        return idCity;
    }

    public static int getUserId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(REGISTER_PREF, Context.MODE_PRIVATE);

        int k = preferences.getInt(REGISTER_ID, -1);
        if (k == -1)
            new Exception("Bad register id").printStackTrace();
        return k;
    }

    public static User getRegUser(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(REGISTER_PREF, Context.MODE_PRIVATE);

        String k = preferences.getString(REGISTER_USER, "");
        User user;
        user = new Gson().fromJson(k, User.class);

        return user;
    }

    public static void savePartner(Context context, PartnerItem partner) {
        SharedPreferences preferences = context.getSharedPreferences(REGISTER_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(PARTNER_ID, partner.idPartner);
        editor.putString(PARTNER, new Gson().toJson(partner));

        editor.apply();
    }

    public static void clearUsers(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(REGISTER_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(PARTNER_ID, 0);
        editor.putString(PARTNER, "");
        editor.putInt(REGISTER_ID, 0);
        editor.putString(REGISTER_USER, "");

        editor.apply();
    }

    public static int getPartnerId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(REGISTER_PREF, Context.MODE_PRIVATE);
        return preferences.getInt(PARTNER_ID, 0);
    }

    public static PartnerItem getPartner(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(REGISTER_PREF, Context.MODE_PRIVATE);
        return new Gson().fromJson(preferences.getString(PARTNER, ""), PartnerItem.class);
    }

    public static void savePartnerLoginPass(Context context, String login, String password) {
        SharedPreferences preferences = context.getSharedPreferences(REGISTER_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PARTNER_LOGIN, login);
        editor.putString(PARTNER_PASSWORD, password);

        editor.apply();
    }

    public static String getPartnerLogin(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(REGISTER_PREF, Context.MODE_PRIVATE);
        return preferences.getString(PARTNER_LOGIN, "");
    }

    public static String getPartnerPass(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(REGISTER_PREF, Context.MODE_PRIVATE);
        return preferences.getString(PARTNER_PASSWORD, "");
    }

    public static String getPartnerToken(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(REGISTER_PREF, Context.MODE_PRIVATE);
        return preferences.getString(PARTNER_TOKEN, "");
    }

    public static String parseError(String serverError) {
        if (!TextUtils.isEmpty(serverError))
            return new Gson().fromJson(serverError, ServerError.class).getErrorMessage();
        else
            return "Ошибка подключения к сети";
    }
}
