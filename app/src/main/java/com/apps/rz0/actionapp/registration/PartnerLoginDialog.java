package com.apps.rz0.actionapp.registration;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.model.register.PartnerItem;
import com.apps.rz0.actionapp.api.model.register.PartnerWrapper;
import com.apps.rz0.actionapp.utils.StaticStrings;
import com.apps.rz0.actionapp.utils.ThemesUtils;
import com.apps.rz0.actionapp.utils.Utils;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Александр on 05.07.2015.
 */
public class PartnerLoginDialog extends DialogFragment {
    public static String TAG = "partner";
    private EditText mail;
    private EditText password;
    private AQuery aq;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.partner_login_dialog_layout, container, false);
        aq = new AQuery(view);
        aq.id(R.id.toolbar_view_title).text("Вход для партнеров");
        aq.id(R.id.progress).gone();
        mail = aq.id(R.id.mail_edittext).getEditText();
        password = aq.id(R.id.password_edittext).getEditText();

        aq.id(R.id.forget_password_partner).getTextView().setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        aq.id(R.id.login_partner_button).getTextView().setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        aq.id(R.id.forget_password_partner).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgetPassDialog dialog = new ForgetPassDialog();
                dialog.show(getActivity().getSupportFragmentManager(), ForgetPassDialog.TAG);
            }
        });

        aq.id(R.id.auth_partner_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkFields())
                    return;
                aq.id(R.id.progress).visible();
                Api.get.authPartner(mail.getText().toString(), password.getText().toString(), new Callback<PartnerWrapper>() {
                    @Override
                    public void success(PartnerWrapper partnerWrapper, Response response) {
                        aq.id(R.id.progress).gone();
                        PartnerItem item = partnerWrapper.partner;
                        if (item == null || TextUtils.isEmpty(item.companyName) && TextUtils.isEmpty(item.description)) {
                            PartnerMoreDialog dialog = new PartnerMoreDialog();
                            dialog.show(getActivity().getSupportFragmentManager(), PartnerMoreDialog.TAG);
                        } else {
                            getActivity().sendBroadcast(new Intent(StaticStrings.UPDATE_DRAWER_RECEIVER));
                        }
                        Utils.clearUsers(getActivity());
                        Utils.savePartnerLoginPass(getActivity(), mail.getText().toString(), password.getText().toString());
                        Utils.savePartner(getActivity(), partnerWrapper.partner);
                        Api.get.updateToken(mail.getText().toString(), password.getText().toString());
                        Utils.dismissDialog(PartnerLoginDialog.this);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        aq.id(R.id.progress).gone();
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        aq.id(R.id.register_partner_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PartnerRegisterDialog dialogFragment = new PartnerRegisterDialog();
                dialogFragment.show(getActivity().getSupportFragmentManager().beginTransaction(), "partner_more");
            }
        });
        aq.id(R.id.login_partner_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginDialog dialogFragment = new LoginDialog();
                dialogFragment.setCancelable(false);
                dialogFragment.show(getActivity().getSupportFragmentManager().beginTransaction(), "register_user");
                Utils.dismissDialog(PartnerLoginDialog.this);
            }
        });

        ThemesUtils.setToolbarColor(getActivity(), aq.id(R.id.toolbar_view).getView());

        return view;
    }

    private boolean checkFields() {
        mail.setError(null);
        password.setError(null);
        if (TextUtils.isEmpty(password.getText())) {
            password.setError("Введите пароль");
            return false;
        }
        if (TextUtils.isEmpty(mail.getText())) {
            mail.setError("Введите e-mail");
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(mail.getText()).matches()) {
            mail.setError("Неверный формат e-mail");
            return false;
        }
        return true;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }
}
