package com.apps.rz0.actionapp.web;


import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.apps.rz0.actionapp.utils.StaticStrings;

public class WebViewInterface {
    Activity activity;

    public WebViewInterface(Activity activity) {
        this.activity = activity;
    }

    @JavascriptInterface
    public void showToast(String message) {
        Log.d("TAG, ", message);
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
        activity.finish();
        activity.sendBroadcast(new Intent(StaticStrings.DISMISS_RECEIVER));
    }
}
