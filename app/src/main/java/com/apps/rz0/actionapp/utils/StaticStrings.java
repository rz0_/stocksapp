package com.apps.rz0.actionapp.utils;

/**
 * Класс для хранения используемым статических переменных
 */
public class StaticStrings {
    public static String CURRENT_SHARED_PREF = "current_pref";
    public static String CURRENT_CITY_ID = "current_city_id";
    public static String CURRENT_USER_SERVER_ID = "current_user_id";
    public static String CURRENT_CITY_NAME = "current_city_name";
    public static String SELECTED_ACTION = "selected_actions";
    public static String UNSELECTED_ACTION = "unselected_actions";
    public static String IS_FIRST_START = "is_first";
    public static String UPDATE_DRAWER_RECEIVER = "update_drawer_receiver";
    public static String DISMISS_RECEIVER = "dismiss_receiver";

    public static String SETTINGS_SHARED_PREF = "settings_pref";
    public static String IS_PUSH_ON = "is_push_on";
    public static String IS_SMS_ON = "is_sms_on";
    public static final String GOOGLE_PROJECT_ID = "791188776552";

    @Deprecated
    public static String SERVER_URL = "http://www.discount.beautyletter.ru/";
    public static String SERVER_URL_2 = "http://www.discount.beautyletter.ru";//TODO правильно использовать этот адрес

}
