package com.apps.rz0.actionapp.api.register;

import android.content.Context;
import android.text.TextUtils;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.apps.rz0.actionapp.GsonTransformer;
import com.apps.rz0.actionapp.StocksApplication;
import com.apps.rz0.actionapp.api.CallbackApi2;
import com.apps.rz0.actionapp.api.model.RegisterId;
import com.apps.rz0.actionapp.api.model.SaveCityResponce;
import com.apps.rz0.actionapp.api.model.User;
import com.apps.rz0.actionapp.api.model.register.PartnerWrapper;
import com.apps.rz0.actionapp.api.model.stock.FilteredImage;
import com.apps.rz0.actionapp.api.model.stock.PayWrapper;
import com.apps.rz0.actionapp.utils.StaticStrings;
import com.apps.rz0.actionapp.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.File;
import java.util.HashMap;

/**
 * Created by Александр on 16.08.2015.
 */
public class RegisterApi {

    Context context;
    AQuery aq;
    private byte[] b;

    public RegisterApi(Context context) {
        this.context = context;
        aq = new AQuery(context);
    }

    public void register(String phone, final CallbackApi2<RegisterId> callback) {
        String url = StaticStrings.SERVER_URL + "api/clientRegister";
        HashMap<String, Object> params = new HashMap<>();

        params.put("phone", phone);
        params.put("device_id", Utils.getDeviceId(aq.getContext()));
        params.put("reg_id", Utils.getRegPushId(aq.getContext()));
        params.put("phone_os", "android");

        aq.transformer(new GsonTransformer()).ajax(url, params, RegisterId.class, new AjaxCallback<RegisterId>() {
            @Override
            public void callback(String url, RegisterId object, AjaxStatus status) {
                if (status.getCode() == 200) {
                    if (callback != null)
                        callback.onSuccess(object);
                } else {
                    if (callback != null)
                        callback.onError(Utils.parseError(status.getError()));
                }
            }
        });
    }

    public void activateUser(String phone, String code, final CallbackApi2<RegisterId> callback) {
        String url = StaticStrings.SERVER_URL + "api/activateClient";
        HashMap<String, Object> params = new HashMap<>();

        params.put("phone", phone);
        params.put("reg_id", Utils.getRegPushId(aq.getContext()));
        params.put("code", code);

        aq.transformer(new GsonTransformer()).ajax(url, params, RegisterId.class, new AjaxCallback<RegisterId>() {
            @Override
            public void callback(String url, RegisterId object, AjaxStatus status) {
                if (status.getCode() == 200) {
                    if (callback != null)
                        callback.onSuccess(object);
                } else {
                    if (callback != null)
                        callback.onError(Utils.parseError(status.getError()));
                }
            }
        });
    }

    public void fillClient(User user, String city, final CallbackApi2<SaveCityResponce> callback) {
        String url = StaticStrings.SERVER_URL + "api/fillClientProfile";
        HashMap<String, Object> params = new HashMap<>();

        params.put("reg_id", Utils.getRegPushId(aq.getContext()));
        params.put("first_name", user.getName());
        params.put("last_name", user.getSurname());
        params.put("city", city);
        if (!TextUtils.isEmpty(user.getAvatarFile()))
            params.put("avatar", new File(user.getAvatarFile()));
        else if (!TextUtils.isEmpty(user.getAvatar())) {
            params.put("avatar", user.getAvatar());
        }
        aq.transformer(new GsonTransformer()).ajax(url, params, SaveCityResponce.class, new AjaxCallback<SaveCityResponce>() {
            @Override
            public void callback(String url, SaveCityResponce object, AjaxStatus status) {
                if (status.getCode() == 200) {
                    if (callback != null)
                        callback.onSuccess(object);
                } else {
                    if (callback != null)
                        callback.onError("");
//                        callback.onError(Utils.parseError(status.getError()));
                }
            }
        });
    }

    public void fillPartner(String companyName, String description, String avatar, long cityId,
                            final CallbackApi2<PartnerWrapper> callback) {
        String url = StaticStrings.SERVER_URL + "api/fillPartnerInfo";
        HashMap<String, Object> params = new HashMap<>();

        params.put("company_name", companyName);
        params.put("description", description);
        params.put("token", StocksApplication.partnerToken);
        params.put("city_id", cityId);
        params.put("partner_id", Utils.getPartnerId(aq.getContext()));
        if (!TextUtils.isEmpty(avatar))
            params.put("avatar", new File(avatar));
        aq.transformer(new GsonTransformer()).ajax(url, params, PartnerWrapper.class, new AjaxCallback<PartnerWrapper>() {
            @Override
            public void callback(String url, PartnerWrapper object, AjaxStatus status) {
                if (status.getCode() == 200) {
                    if (callback != null)
                        callback.onSuccess(object);
                } else {
                    if (callback != null)
                        callback.onError(Utils.parseError(status.getError()));
                }
            }
        });
    }

    public void addStock(String filePath, String dateStart, String dateEnd, int idShop,
                         int discountSize, String shortDescription, String fullDescription,
                         String categories, final CallbackApi2<PayWrapper> callback) {
        String url = StaticStrings.SERVER_URL + "api/addStock";
        HashMap<String, Object> params = new HashMap<>();

        params.put("token", StocksApplication.partnerToken);
        if (!TextUtils.isEmpty(filePath))
            params.put("file", new File(filePath));
        params.put("shop_id", idShop);
        params.put("date_start", dateStart);
        params.put("date_end", dateEnd);
        params.put("discount", discountSize);
        params.put("short_descr", shortDescription);
        params.put("message", fullDescription);
        params.put("tags", categories);

        aq.transformer(new GsonTransformer()).ajax(url, params, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String object, AjaxStatus status) {
                if (status.getCode() == 200) {
                    if (callback != null)
                        try {
                            callback.onSuccess(new Gson().fromJson(object, PayWrapper.class));
                        } catch (JsonSyntaxException e) {
                            callback.onError(Utils.parseError(object));
                            e.printStackTrace();
                        }
                } else {
                    if (callback != null)
                        callback.onError(Utils.parseError(status.getError()));
                }
            }
        });
    }

    public void loadFilterImage(String filePath, final CallbackApi2<FilteredImage> callback) {
        String url = StaticStrings.SERVER_URL_2 + "/api/uploadImage";
        HashMap<String, Object> params = new HashMap<>();

        params.put("token", StocksApplication.partnerToken);
        if (!TextUtils.isEmpty(filePath))
            params.put("image", new File(filePath));
        aq.transformer(new GsonTransformer()).ajax(url, params, FilteredImage.class, new AjaxCallback<FilteredImage>() {
            @Override
            public void callback(String url, FilteredImage object, AjaxStatus status) {
                if (status.getCode() == 200) {
                    if (callback != null)
                        callback.onSuccess(object);
                } else {
                    if (callback != null)
                        callback.onError(Utils.parseError(status.getError()));
                }
            }
        });
    }

}
