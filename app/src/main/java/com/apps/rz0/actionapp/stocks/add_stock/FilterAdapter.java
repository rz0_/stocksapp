package com.apps.rz0.actionapp.stocks.add_stock;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.api.model.stock.FilterItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FilterAdapter extends RecyclerView.Adapter {

    ArrayList<FilterItem> items;
    View.OnClickListener filterClick;

    public FilterAdapter(ArrayList<FilterItem> items, View.OnClickListener filterClick) {
        this.items = items;
        this.filterClick = filterClick;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_item, parent, false);
        FilterViewHolder holder = new FilterViewHolder(view);
        holder.itemView.setOnClickListener(filterClick);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        FilterViewHolder holder = (FilterViewHolder) viewHolder;
        holder.setup(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class FilterViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title;

        public FilterViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            title = (TextView) itemView.findViewById(R.id.title);
        }

        public void setup(FilterItem item) {
            Picasso.with(itemView.getContext())
                    .load(item.url)
                    .into(image);

            title.setText(item.title);
            itemView.setTag(item.title);
        }
    }
}
