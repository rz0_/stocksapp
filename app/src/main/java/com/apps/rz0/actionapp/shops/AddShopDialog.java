package com.apps.rz0.actionapp.shops;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.StocksApplication;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.model.shops.ShopItem;
import com.apps.rz0.actionapp.categories.CategoriesShopActivity;
import com.apps.rz0.actionapp.choose_dialog.CityItem;
import com.apps.rz0.actionapp.registration.CityArrayAdapter;
import com.apps.rz0.actionapp.registration.MaskedWatcher;
import com.apps.rz0.actionapp.utils.ThemesUtils;
import com.apps.rz0.actionapp.utils.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AddShopDialog extends DialogFragment {
    public static final String TAG = "add_path_dialog";
    public static final String ITEM = "add_path_item";
    public static final String IS_EDIT_ITEM = "is_edit_item";

    public static final int CATEGORIES_CODE = 100;

    EditText title;
    AutoCompleteTextView street;
    EditText comment;
    private Spinner citiesSpinner;
    private ShopItem item;
    private boolean isEdit;
    private Callback<Response> callback;
    private UpdateShopsInterface updateShopsInterface;
    private String categories;
    private EditText phoneEditText;

    public AddShopDialog setUpdateShopsInterface(UpdateShopsInterface updateShopsInterface) {
        this.updateShopsInterface = updateShopsInterface;
        return this;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);
        Api.get.updateToken(Utils.getPartnerLogin(getActivity()), Utils.getPartnerPass(getActivity()));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_shop_layout, container, false);
        final AQuery aq = new AQuery(view);
        isEdit = getArguments().getBoolean(IS_EDIT_ITEM);
        if (isEdit)
            aq.id(R.id.toolbar_view_title).text("Редактирование отдела");
        else
            aq.id(R.id.toolbar_view_title).text("Новый отдел");

        aq.id(R.id.arrow_home).visible().clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCancelable())
                    dismiss();
            }
        });
        citiesSpinner = aq.id(R.id.cities_spinner).getSpinner();
        title = aq.id(R.id.title_shop).getEditText();
        street = (AutoCompleteTextView) aq.id(R.id.street_shop).getView();
        comment = aq.id(R.id.comment_shop).getEditText();

        phoneEditText = aq.id(R.id.phone_edittext).getEditText();
        phoneEditText.addTextChangedListener(new MaskedWatcher(phoneEditText, "+7(###) ###-##-##"));

        Api.get.getAllCities(new Api.OnDataGet<ArrayList<CityItem>>() {
            @Override
            public void getData(ArrayList<CityItem> success) {
                if (success == null || success.size() == 0)
                    return;
                citiesSpinner.setAdapter(new CityArrayAdapter(getActivity(), R.layout.spinner_main_layout, success));
                if (isEdit) {
                    int selectedCity = 0;
                    for (int i = 0; i < success.size(); i++) {
                        if (success.get(i).getCityTitle().equalsIgnoreCase(item.city)) {
                            selectedCity = i;
                            break;
                        }
                    }
                    citiesSpinner.setSelection(selectedCity);
                }
            }
        });

        aq.id(R.id.add_shop_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkFields())
                    return;
                callback = new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        dismiss();
                        if (updateShopsInterface != null)
                            updateShopsInterface.updateShops();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                };
                if (!isEdit) {
                    Api.get.addShop(processParams(), callback);
                } else {
                    Api.get.editShop(processParams(), callback);
                }
            }
        });

        if (isEdit) {
            item = new Gson().fromJson(getArguments().getString(ITEM), ShopItem.class);
            title.setText(item.title);
            street.setText(item.address);
            comment.setText(item.additional);
            phoneEditText.setText(item.phone);
        }

        aq.id(R.id.choose_categories_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CategoriesShopActivity.class);
                if (isEdit)
                    intent.putExtra(CategoriesShopActivity.CATEGORIES, item.categories);
                startActivityForResult(intent, CATEGORIES_CODE);
            }
        });

        setStreetServer();
        ThemesUtils.setToolbarColor(getActivity(), aq.id(R.id.toolbar_view).getView());

        return view;
    }

    private Map<String, String> processParams() {
        Map<String, String> params = new HashMap<>();
        params.put("token", StocksApplication.partnerToken);
        if (isEdit)
            params.put("shop_id", String.valueOf(item.idShop));
        params.put("title", title.getText().toString());
        params.put("city_id", String.valueOf(citiesSpinner.getSelectedItemId()));
        params.put("address", street.getText().toString());
        params.put("phone", phoneEditText.getText().toString());
        params.put("additional", comment.getText().toString());
        if (!TextUtils.isEmpty(categories))
            params.put("categories", categories.substring(1, categories.length() - 1));

        return params;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CATEGORIES_CODE && resultCode == Activity.RESULT_OK) {
            categories = data.getStringExtra(CategoriesShopActivity.CATEGORIES);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setStreetServer() {
        street.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 4)
                    return;
                getStreets(s.toString());
            }
        });
    }

    private void getStreets(String query) {
        Api.get.getStreets(((CityItem) citiesSpinner.getSelectedItem()).getCityTitle(), query,
                new Api.OnDataGet<ArrayList<String>>() {
                    @Override
                    public void getData(ArrayList<String> success) {
                        street.setAdapter(new StreetArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, success));
                    }
                });
    }

    private boolean checkFields() {
        title.setError(null);
        street.setError(null);
        if (TextUtils.isEmpty(title.getText())) {
            title.setError("Введите название");
            return false;
        }
        if (TextUtils.isEmpty(street.getText())) {
            street.setError("Введите улицу");
            return false;
        }
        return true;
    }

}
