package com.apps.rz0.actionapp.stocks;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.apps.rz0.actionapp.R;
import com.vk.sdk.dialogs.VKShareDialog;

import java.io.File;

public class ShareDialog extends Dialog {
    StockItem item;
    ActionBarActivity activity;

    public ShareDialog(ActionBarActivity activity, StockItem item) {
        super(activity);
        this.item = item;
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.share_dialog_layout);
        Button share = (Button) findViewById(R.id.share_button);
        Button map = (Button) findViewById(R.id.map_button);
        AQuery aq = new AQuery(getContext());
        File ext = Environment.getExternalStorageDirectory();
        final File target = new File(ext, "photo.jpg");
        aq.download(item.getImage(), target, new AjaxCallback<File>());
        final String googleLink = "https://play.google.com/store/apps/details?id=com.apps.rz0.actionapp&hl=ru";

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                vkontaktePublish(googleLink + "\n" +
//                        "Я участвую в акции Центр Скидки: " + item.getShort_descr(), item.getImage(), "");
                Intent intent = new Intent(Intent.ACTION_SEND);
                String text = googleLink + "\n" +
                        "Я участвую в акции Центр Скидки: " + item.getShort_descr();
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, text);
                intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(target));
                v.getContext().startActivity(Intent.createChooser(intent, "Поделиться скидкой"));
                dismiss();
            }
        });

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:" + item.getLat() +
                        "," + item.getLon() + "?q=" + item.getLat() + "," +
                        item.getLon() + "(" + item.getCompany().getTitle() + ")"));
                v.getContext().startActivity(intent);

                dismiss();
            }
        });
    }

    public final void vkontaktePublish(String message, String link, String linkName) {
        new VKShareDialog()
                .setText(message)
                .setAttachmentLink(linkName, link)
                .setShareDialogListener(new VKShareDialog.VKShareDialogListener() {
                    @Override
                    public void onVkShareComplete(int postId) {

                    }

                    @Override
                    public void onVkShareCancel() {
                    }
                }).show(activity.getSupportFragmentManager(), "VK_SHARE_DIALOG");
    }
}
