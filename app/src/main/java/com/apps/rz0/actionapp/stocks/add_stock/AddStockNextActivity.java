package com.apps.rz0.actionapp.stocks.add_stock;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.StocksApplication;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.CallbackApi2;
import com.apps.rz0.actionapp.api.model.shops.ShopItem;
import com.apps.rz0.actionapp.api.model.stock.PayItem;
import com.apps.rz0.actionapp.api.model.stock.PayWrapper;
import com.apps.rz0.actionapp.shops.AddShopDialog;
import com.apps.rz0.actionapp.shops.UpdateShopsInterface;
import com.apps.rz0.actionapp.utils.StaticStrings;
import com.apps.rz0.actionapp.utils.ThemesUtils;
import com.apps.rz0.actionapp.utils.Utils;
import com.apps.rz0.actionapp.web.WebBrowserActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AddStockNextActivity extends ActionBarActivity {

    public final static String SHORT_DESCRIPTION_STOCK = "name_stock";
    public final static String DESCRIPTION_STOCK = "description_stock";
    public final static String PATH_IMAGE_STOCK = "path_stock";
    public final static String TAG = "add_next_dialog";

    AQuery aq;
    ArrayList<Integer> stockSize = new ArrayList<>();
    Spinner stockSizeSpinner, shopSpinner;
    private View.OnClickListener editClick;
    private EditText startStock, endStock;

    BroadcastReceiver dismissReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };
    private EditText categories;

    private void fillStockSize() {
        stockSize.add(5);
        stockSize.add(10);
        stockSize.add(15);
        stockSize.add(20);
        stockSize.add(25);
        stockSize.add(30);
        stockSize.add(35);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_stock_next);

        aq = new AQuery(this);
        final Calendar calendar = new GregorianCalendar(Locale.getDefault());
        fillStockSize();
        stockSizeSpinner = aq.id(R.id.stock_size_spinner).getSpinner();
        shopSpinner = aq.id(R.id.shops_spinner).getSpinner();
        startStock = aq.id(R.id.start_stock).getEditText();
        endStock = aq.id(R.id.end_stock).getEditText();
        categories = aq.id(R.id.categories).getEditText();
        aq.id(R.id.progress).gone();

        stockSizeSpinner.setAdapter(new StockSizeArrayAdapter(this, R.layout.spinner_main_layout, stockSize));

        editClick = new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                DatePickerDialog dialog = new DatePickerDialog(AddStockNextActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        ((EditText) v).setText(dayOfMonth + "." + monthOfYear + "." + year);
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                dialog.show();
            }
        };
        aq.id(R.id.start_stock).clicked(editClick);
        aq.id(R.id.end_stock).clicked(editClick);

        aq.id(R.id.pay_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payStock();
            }
        });

        ThemesUtils.setToolbar(this, "Добавить акцию");
    }

    private void payStock() {
        if (!checkFields())
            return;
        aq.id(R.id.progress).visible();

        Api.get.addStock(processParams(), new Callback<PayWrapper>() {
            @Override
            public void success(PayWrapper payWrapper, Response response) {
                aq.id(R.id.progress).gone();
                Intent intent = new Intent(AddStockNextActivity.this, WebBrowserActivity.class);
                intent.putExtra(WebBrowserActivity.WEB_URL, payWrapper.message.payLink);
                startActivity(intent);
            }

            @Override
            public void failure(RetrofitError error) {
                aq.id(R.id.progress).gone();
                Toast.makeText(AddStockNextActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private Map<String, String> processParams(){
        Map<String, String> params = new HashMap<>();
        params.put("token", StocksApplication.partnerToken);
        params.put("image", getIntent().getStringExtra(PATH_IMAGE_STOCK));
        params.put("shop_id", String.valueOf(shopSpinner.getSelectedItemId()));
        params.put("date_start", startStock.getText().toString());
        params.put("date_end", endStock.getText().toString());
        params.put("discount", String.valueOf(stockSizeSpinner.getSelectedItemId()));
        params.put("short_descr", getIntent().getStringExtra(SHORT_DESCRIPTION_STOCK));
        params.put("message", getIntent().getStringExtra(DESCRIPTION_STOCK));
        params.put("tags", categories.getText().toString());

        return params;
    }


    private boolean checkFields() {
        if (TextUtils.isEmpty(startStock.getText())) {
            startStock.setError("Введите дату начала");
            return false;
        }
        if (TextUtils.isEmpty(categories.getText())) {
            startStock.setError("Введите категории");
            return false;
        }
        if (TextUtils.isEmpty(endStock.getText())) {
            endStock.setError("Введите дату окончания");
            return false;
        }
        return true;
    }

    private void getPartnerShops() {
        Api.get.getShops(Utils.getPartnerId(this), new Api.OnDataGet<ArrayList<ShopItem>>() {
            @Override
            public void getData(ArrayList<ShopItem> success) {
                if (success == null)
                    return;
                if (success.size() > 0) {
                    shopSpinner.setAdapter(
                            new ShopsArrayAdapter(AddStockNextActivity.this, R.layout.spinner_main_layout, success));
                } else {
                    Toast.makeText(AddStockNextActivity.this, "Для добавления акции требуется добавить отдел",
                            Toast.LENGTH_LONG).show();
                    AddShopDialog dialog = new AddShopDialog();
                    Bundle args = new Bundle();
                    args.putBoolean(AddShopDialog.IS_EDIT_ITEM, false);
                    dialog.setArguments(args);
                    dialog.setCancelable(false);
                    dialog.setUpdateShopsInterface(new UpdateShopsInterface() {
                        @Override
                        public void updateShops() {
                            getPartnerShops();
                        }
                    });
                    dialog.show(getSupportFragmentManager(), AddShopDialog.TAG);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(dismissReceiver, new IntentFilter(StaticStrings.DISMISS_RECEIVER));
        getPartnerShops();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(dismissReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
