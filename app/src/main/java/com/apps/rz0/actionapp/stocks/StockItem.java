package com.apps.rz0.actionapp.stocks;

import com.google.gson.annotations.SerializedName;

/**
 * Один элемент-акция
 */
public class StockItem {
    @SerializedName("stock_id")
    private Integer stockId;
    private String date;
    private String endDate;
    private CompanyItem company;
    private String image;
    private String content;
    private String short_descr;
    private String address;
    private String phone;
    private Double lat;
    private Double lon;
    private String views;
    private Integer likes;
    @SerializedName("comments_num")
    private Integer commentsNum;
    private String discount;
    @SerializedName("days_left")
    private String daysLeft;
    @SerializedName("company_avatar")
    private String companyAvatar;
    @SerializedName("is_liked")
    private boolean isLiked;

    @SerializedName("shop_id")
    public int idShop;

    public StockItem setLikes(Integer likes) {
        this.likes = likes;
        return this;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public StockItem setIsLiked(boolean isLiked) {
        this.isLiked = isLiked;
        return this;
    }

    public Integer getStockId() {
        return stockId;
    }

    public String getDate() {
        return date;
    }

    public String getEndDate() {
        return endDate;
    }

    public CompanyItem getCompany() {
        return company;
    }

    public String getImage() {
        return image;
    }

    public String getContent() {
        return content;
    }

    public String getShort_descr() {
        return short_descr;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public String getViews() {
        return views;
    }

    public Integer getLikes() {
        return likes;
    }

    public Integer getCommentsNum() {
        return commentsNum;
    }

    public String getDiscount() {
        return discount;
    }

    public String getDaysLeft() {
        return daysLeft;
    }

    public String getCompanyAvatar() {
        return companyAvatar;
    }
}
