package com.apps.rz0.actionapp;

import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.Transformer;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

public class GsonTransformer implements Transformer {

    public <T> T transform(String url, Class<T> type, String encoding, byte[] data, AjaxStatus status) {
        Gson g = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        T t;
        try {
            String query = new String(data);
            t = g.fromJson(query, type);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return null;
        }
        return t;
    }
}
