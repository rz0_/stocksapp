package com.apps.rz0.actionapp.social;

import android.location.Location;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Александр on 19.08.2015.
 */
public class OKUser {
    private String uid;
    private String birthday;
    private Integer age;
    private String name;
    private String locale;
    private String gender;
    private OKLocation location;
    private String online;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("has_email")
    private Boolean hasEmail;
    @SerializedName("pic_1")
    private String pic1;
    @SerializedName("pic_2")
    private String pic2;

    public String getUid() {
        return uid;
    }

    public String getBirthday() {
        return birthday;
    }

    public Integer getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getLocale() {
        return locale;
    }

    public String getGender() {
        return gender;
    }

    public OKLocation getLocation() {
        return location;
    }

    public String getOnline() {
        return online;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Boolean getHasEmail() {
        return hasEmail;
    }

    public String getPic1() {
        return pic1;
    }

    public String getPic2() {
        return pic2;
    }
}
