package com.apps.rz0.actionapp.stocks.add_stock;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.BitmapAjaxCallback;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.StocksApplication;
import com.apps.rz0.actionapp.api.Api;
import com.apps.rz0.actionapp.api.CallbackApi2;
import com.apps.rz0.actionapp.api.model.stock.FilterItem;
import com.apps.rz0.actionapp.api.model.stock.FilteredImage;
import com.apps.rz0.actionapp.utils.ThemesUtils;

import java.util.ArrayList;

public class FilterActivity extends ActionBarActivity {
    public static final String IMAGE = "image";
    public static final String IMAGE_URL = "image_url";
    private AQuery aq;
    private String imageName;
    private String filteredImage;
    private String imageUrl;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        aq = new AQuery(this);
        aq.id(R.id.progress).visible();
        aq.id(R.id.content).gone();

        StocksApplication.registerApi.loadFilterImage(getIntent().getStringExtra(IMAGE), new CallbackApi2<FilteredImage>() {
            @Override
            public void onSuccess(FilteredImage object) {
                aq.id(R.id.progress).gone();
                aq.id(R.id.content).visible();
                imageName = object.name;
                imageUrl = object.url;
                BitmapAjaxCallback cb = new BitmapAjaxCallback();
                cb.url(object.url).rotate(true);
                aq.id(R.id.choosed_image).image(cb);
            }

            @Override
            public void onError(String error) {
                aq.id(R.id.progress).gone();
                Toast.makeText(FilterActivity.this, error, Toast.LENGTH_LONG).show();
            }
        });

        aq.id(R.id.next_button_stock).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                if (TextUtils.isEmpty(filteredImage))
                    intent.putExtra(IMAGE, imageName);
                else
                    intent.putExtra(IMAGE, filteredImage);
                intent.putExtra(IMAGE_URL, imageUrl);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        final View.OnClickListener filterClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.setEnabled(false);
                Api.get.filterImage(imageName, (String) v.getTag(), new Api.OnDataGet<FilteredImage>() {
                    @Override
                    public void getData(FilteredImage success) {
                        if (success == null)
                            return;
                        recyclerView.setEnabled(true);
                        BitmapAjaxCallback cb = new BitmapAjaxCallback();
                        cb.url(success.url).rotate(true);
                        aq.id(R.id.choosed_image).image(success.url);
                        filteredImage = success.name;
                        imageUrl = success.url;
                    }
                });
            }
        };

        recyclerView = (RecyclerView) aq.id(R.id.filter_recyclerView).getView();
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        Api.get.getFilters(new Api.OnDataGet<ArrayList<FilterItem>>() {
            @Override
            public void getData(ArrayList<FilterItem> success) {
                if(success == null)
                    return;
                recyclerView.setAdapter(new FilterAdapter(success, filterClick));
            }
        });


        ThemesUtils.setToolbar(this, "Фильтр");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
    }
}
