package com.apps.rz0.actionapp.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.apps.rz0.actionapp.R;

public class ThemesUtils {
    private final static String THEMES_PREFERENCES = "theme_pref";
    private final static String CUSTOM_COLOR = "color";
    public static String DEFAULT_COLOR = "a55168";

    public static void setToolbarColor(ActionBarActivity activity) {
        SharedPreferences themesPreferences = activity.getSharedPreferences(THEMES_PREFERENCES, Context.MODE_PRIVATE);
        String color = themesPreferences.getString(CUSTOM_COLOR, DEFAULT_COLOR);
        activity.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#" + color)));
    }

    public static void setToolbar(ActionBarActivity activity, String title) {
        Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        activity.getSupportActionBar().setHomeButtonEnabled(true);
        activity.getSupportActionBar().setTitle(title);
        activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_button);
        ThemesUtils.setToolbarColor(activity);
    }

    public static void setToolbarColor(Context context, View view) {
        SharedPreferences themesPreferences = context.getSharedPreferences(THEMES_PREFERENCES, Context.MODE_PRIVATE);
        String color = themesPreferences.getString(CUSTOM_COLOR, DEFAULT_COLOR);
        view.setBackground(new ColorDrawable(Color.parseColor("#" + color)));
    }

    public static void saveColorTheme(ActionBarActivity activity, String color) {
        SharedPreferences themesPreferences = activity.getSharedPreferences(THEMES_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = themesPreferences.edit();
        editor.putString(CUSTOM_COLOR, color);
        editor.apply();
        setToolbarColor(activity);
    }
}
