package com.apps.rz0.actionapp.api.register;

import android.content.Context;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.apps.rz0.actionapp.GsonTransformer;
import com.apps.rz0.actionapp.utils.StaticStrings;

import java.util.HashMap;

/**
 * Created by Александр on 24.08.2015.
 */
public class StockApi {
    Context context;
    AQuery aq;

    public StockApi(Context context) {
        this.context = context;
        this.aq = new AQuery(context);
    }


    public void addViewedAction(int idMessage) {
        String url = StaticStrings.SERVER_URL_2 + "/api/viewMessage";
        HashMap<String, Object> params = new HashMap<>();

        params.put("message_id", idMessage);

        aq.transformer(new GsonTransformer()).ajax(url, params, String.class, new AjaxCallback<String>());
    }


}
