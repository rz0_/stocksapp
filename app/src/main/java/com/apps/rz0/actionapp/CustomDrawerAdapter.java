package com.apps.rz0.actionapp;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.BitmapAjaxCallback;
import com.apps.rz0.actionapp.api.model.User;
import com.apps.rz0.actionapp.api.model.register.PartnerItem;
import com.apps.rz0.actionapp.utils.StaticStrings;
import com.apps.rz0.actionapp.utils.Utils;

import java.util.List;

/**
 * Класс бокового меню
 */
public class CustomDrawerAdapter extends ArrayAdapter<DrawerItem> {
    Context context;
    AQuery aq;
    private final int DIVIDER_TYPE = 0;
    private final int ITEM_TYPE = 1;
    private final int PROFILE_TYPE = 2;
    private final int ADD_STOCK_TYPE = 3;

    public CustomDrawerAdapter(Context context, int resource, List<DrawerItem> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        switch (getItemViewType(position)) {
            case ITEM_TYPE: {
                convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.drawer_item_layout, parent, false);
                aq = new AQuery(convertView);
                aq.id(R.id.itemTitle).text(getItem(position).title);
                if (((ListView) parent).isItemChecked(position)) {
                    aq.id(R.id.arrow).image(R.drawable.ic_navigation_chevron_right_selected);
                } else {
                    aq.id(R.id.arrow).image(R.drawable.ic_navigation_chevron_right);
                }
                aq.id(R.id.itemIcon).image(getItem(position).idIcon);
                if (!getItem(position).isArrowVisible) {
                    aq.id(R.id.arrow).gone();
                    aq.id(R.id.divider).gone();
                }
                break;
            }
            case ADD_STOCK_TYPE: {
                convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.drawer_add_stock_layout, parent, false);
                aq = new AQuery(convertView);
                aq.id(R.id.itemTitle).text(getItem(position).title);
                aq.id(R.id.itemIcon).image(getItem(position).idIcon);
                break;
            }
            case DIVIDER_TYPE: {
                convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.divider_drawer, parent, false);
                break;
            }

            case PROFILE_TYPE: {
                convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.drawer_profile_layout, parent, false);
                AQuery aq = new AQuery(convertView);
                User user = Utils.getRegUser(aq.getContext());
                PartnerItem partner = Utils.getPartner(aq.getContext());
                BitmapAjaxCallback cb = new BitmapAjaxCallback();
                if (user != null) {
                    cb.url(StaticStrings.SERVER_URL_2 + user.getAvatar()).rotate(true).fallback(R.drawable.ic_cap_menu);
                    aq.id(R.id.user_avatar).image(cb);
                    aq.id(R.id.user_counter).gone();
                    if (!TextUtils.isEmpty(user.getName()))
                        aq.id(R.id.user_name).text(user.getName() + " " + user.getSurname());
                    if (!TextUtils.isEmpty(user.getCity()))
                        aq.id(R.id.user_city).text(user.getCity());
                } else if (partner != null) {
                    cb.url(StaticStrings.SERVER_URL_2 + partner.avatar).rotate(true).fallback(R.drawable.ic_cap_menu);
                    aq.id(R.id.user_avatar).image(cb);
                    aq.id(R.id.user_counter).gone();
                    if (!TextUtils.isEmpty(partner.companyName))
                        aq.id(R.id.user_name).text(partner.companyName);
                    if (!TextUtils.isEmpty(partner.saleCount))
                        aq.id(R.id.user_city).text(partner.saleCount);
                }
                break;
            }
        }
        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 4;
    }

    @Override
    public boolean isEnabled(int position) {
        return getItemViewType(position) != DIVIDER_TYPE;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return PROFILE_TYPE;
        if (TextUtils.isEmpty(getItem(position).title))
            return DIVIDER_TYPE;
        if (getItem(position).title.equalsIgnoreCase(MainActivity.ADD_STOCK))
            return ADD_STOCK_TYPE;
        else
            return ITEM_TYPE;
    }
}
