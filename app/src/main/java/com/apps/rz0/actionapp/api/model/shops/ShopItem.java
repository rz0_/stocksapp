package com.apps.rz0.actionapp.api.model.shops;

import com.google.gson.annotations.SerializedName;

public class ShopItem {
    public String title;
    public String city;
    @SerializedName("city_id")
    public int idCity;
    @SerializedName("shop_id")
    public int idShop;
    public String address;
    public String additional;
    public String phone;
    public String categories;

    @Override
    public String toString() {
        return address;
    }
}
