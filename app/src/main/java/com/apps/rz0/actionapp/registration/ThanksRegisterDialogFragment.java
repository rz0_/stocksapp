package com.apps.rz0.actionapp.registration;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.androidquery.AQuery;
import com.apps.rz0.actionapp.R;
import com.apps.rz0.actionapp.utils.ThemesUtils;
import com.apps.rz0.actionapp.utils.Utils;

/**
 * Created by Александр on 04.07.2015.
 */
public class ThanksRegisterDialogFragment extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.thanks_register_dialog, container, false);
        AQuery aq = new AQuery(view);
        aq.id(R.id.toolbar_view_title).text("Регистрация");
        aq.id(R.id.main_menu_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.getRegUser(getActivity()) == null && Utils.getPartner(getActivity()) == null) {
                    LoginDialog loginDialog = new LoginDialog();
                    loginDialog.setCancelable(false);
                    Bundle args = new Bundle();
                    args.putBoolean(LoginDialog.START_FLAG, true);
                    loginDialog.setArguments(args);
                    loginDialog.show(getActivity().getSupportFragmentManager().beginTransaction(), "register");
                }
                dismiss();
            }
        });
        ThemesUtils.setToolbarColor(getActivity(), aq.id(R.id.toolbar_view).getView());

        return view;
    }
}
