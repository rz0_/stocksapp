package com.apps.rz0.actionapp.social;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import com.apps.rz0.actionapp.api.CallbackApi2;
import com.apps.rz0.actionapp.api.model.SignedUser;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKSdkListener;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKUsersArray;
import com.vk.sdk.dialogs.VKCaptchaDialog;
import com.vk.sdk.dialogs.VKShareDialog;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import ru.ok.android.sdk.Odnoklassniki;
import ru.ok.android.sdk.OkTokenRequestListener;
import ru.ok.android.sdk.util.OkScope;

/**
 * Created by Александр on 31.05.2015.
 */
public class SocialSDK {

    public static String VK_APPLICATION_ID = "4939953";
    public static final String[] VK_SCOPE = new String[]{VKScope.FRIENDS, VKScope.WALL, VKScope.PHOTOS, VKScope.NOHTTPS};
    public static final String VK_TOKEN_KEY = "nqZGl5lxNUQcfAJviOnV";
    private static VKCallback vkCallback = new VKCallback();

    private static CallbackManager facebookCallbackManager = CallbackManager.Factory.create();
    public static final String FB_SCOPE = "id,name,email,first_name,last_name";
//    public static final String FB_SCOPE = "id,name,email,first_name,last_name,user_location";

    protected static final String OK_APP_ID = "1139546624";
    //    protected static final String OK_APP_ID = "125497344";
    protected static final String OK_APP_SECRET = "C8FAA0CA5749184C233001E8";
    //    protected static final String OK_APP_SECRET = "E1B27795E3C2AF1A7B14CB11";
    protected static final String OK_APP_KEY = "CBALBNOEEBABABABA";
    //    protected static final String OK_APP_KEY = "CBABPLHIABABABABA";
    public static final String SOCIAL_NETWORK_TAG_OK = "SocialIntegrationMain.SOCIAL_NETWORK_TAG";
    protected static Odnoklassniki mOdnoklassniki;
    private static String[] okScope = new String[]{
            OkScope.VALUABLE_ACCESS
    };

    public static void initialize(Context context) {
        FacebookSdk.sdkInitialize(context);
        VKSdk.initialize(vkCallback, VK_APPLICATION_ID);
        mOdnoklassniki = Odnoklassniki.createInstance(context, OK_APP_ID, OK_APP_SECRET, OK_APP_KEY);
    }

    public static void authorize(ActionBarActivity activity, SocialNet type, final CallbackApi2<SignedUser> callback) {
        switch (type) {
            case FACEBOOK:
                authFacebook(activity, callback, new DoAfterLogin() {
                    @Override
                    public void run() {
                        JSONObject jsonObject = (JSONObject) map.get("jsonObject");
                        SignedUser user = new SignedUser();
                        user.setName(jsonObject.optString("first_name"))
                                .setSuname(jsonObject.optString("last_name"))
                                .setAvatar("https://graph.facebook.com/" + jsonObject.optString("id") + "/picture?type=large")
                                .setCity(jsonObject.optString("name"));
                        callback.onSuccess(user);
                    }
                });
                break;
            case VKONTAKTE:
                vkCallback.setRequestCallback(callback);
                vkCallback.setDoAfterLogin(new DoAfterLogin() {
                    @Override
                    public void run() {
                        VKApiUserFull userFull = (VKApiUserFull) map.get("userFull");
                        SignedUser user = new SignedUser();
                        user.setName(userFull.first_name)
                                .setSuname(userFull.last_name)
                                .setAvatar(userFull.photo_200)
                                .setCity(userFull.city.title);
                        callback.onSuccess(user);
                    }
                });
                onResume(activity);
                VKSdk.authorize(VK_SCOPE);
                break;
            case ODNOKLASSNIKI:
                mOdnoklassniki.requestAuthorization(activity, true, OkScope.VALUABLE_ACCESS);
                OKCallback okCallback = new OKCallback();
                okCallback.setCallback(callback);
                okCallback.setDoAfterLogin(new DoAfterLogin() {
                    @Override
                    public void run() {
                        OKUser okUser = new Gson().fromJson((String) map.get("OKUser"), OKUser.class);
                        SignedUser user = new SignedUser();
                        user.setName(okUser.getFirstName())
                                .setSuname(okUser.getLastName())
                                .setAvatar(okUser.getPic1())
                                .setCity(okUser.getLocation().getCity());
                        callback.onSuccess(user);
                    }
                });
                mOdnoklassniki.setTokenRequestListener(okCallback);

        }
    }

    private static void authFacebook(Activity activity, final @Nullable CallbackApi2<SignedUser> callback, final DoAfterLogin doAfter) {
        LoginManager.getInstance().registerCallback(facebookCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        getFacebookData(doAfter);
                    }

                    @Override
                    public void onCancel() {
                        Log.d("Facebook", "onCancel");
                        if (callback != null) {
                            callback.onError("cancel");
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d("Facebook", "onError" + exception);
                        if (callback != null) {
                            callback.onError("onError" + exception.toString());
                        }
                    }
                });
        if (AccessToken.getCurrentAccessToken() == null) {
            LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "user_friends", "email"));
        } else {
            getFacebookData(doAfter);
        }
    }

    private static void getFacebookData(final DoAfterLogin doAfter) {
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        doAfter.addObject("jsonObject", object);
                        doAfter.run();
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", FB_SCOPE);
        request.setParameters(parameters);
        request.executeAsync();
    }

    public static void onCreate(ActionBarActivity activity) {
        VKUIHelper.onCreate(activity);
    }

    public static void onResume(ActionBarActivity activity) {
        VKUIHelper.onResume(activity);
    }

    public static void onDestroy(ActionBarActivity activity) {
        VKUIHelper.onDestroy(activity);
    }

    public static void onActivityResult(ActionBarActivity activity, int requestCode, int resultCode, Intent data) {
        if (facebookCallbackManager != null)
            facebookCallbackManager.onActivityResult(requestCode, resultCode, data);

        VKUIHelper.onActivityResult(activity, requestCode, resultCode, data);

        Fragment fragment = activity.getSupportFragmentManager().findFragmentByTag(SOCIAL_NETWORK_TAG_OK);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    static class VKCallback extends VKSdkListener {

        @Nullable
        DoAfterLogin doAfterLogin;
        CallbackApi2<SignedUser> callback;

        public VKCallback setDoAfterLogin(DoAfterLogin doAfterLogin) {
            this.doAfterLogin = doAfterLogin;
            return this;
        }

        @Override
        public void onCaptchaError(VKError captchaError) {
            new VKCaptchaDialog(captchaError).show();
        }

        @Override
        public void onTokenExpired(VKAccessToken expiredToken) {
            VKSdk.authorize(VK_SCOPE);
        }

        public void setRequestCallback(CallbackApi2<SignedUser> callback) {
            this.callback = callback;
        }

        @Override
        public void onAccessDenied(VKError authorizationError) {
            new AlertDialog.Builder(VKUIHelper.getTopActivity())
                    .setMessage(authorizationError.toString())
                    .show();
        }

        @Override
        public void onReceiveNewToken(VKAccessToken newToken) {
            newToken.saveTokenToSharedPreferences(VKUIHelper.getTopActivity(), VK_TOKEN_KEY);
            //           Intent i = new Intent(TeleFMApplication.this, MainActivity.class);
            //           startActivity(i);
            getVKUserData();
        }

        private void getVKUserData() {
            VKRequest vkRequest = new VKRequest("users.get",
                    VKParameters.from(VKApiConst.FIELDS, "sex, bdate, city, country, photo_50, photo_100, photo_200_orig, photo_200," +
                            " photo_400_orig, photo_max, photo_max_orig, photo_id, online, online_mobile, domain, contacts," +
                            "screen_name, maiden_name"),
                    VKRequest.HttpMethod.GET,
                    VKUsersArray.class);
            vkRequest.executeWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    try {
                        VKUsersArray usersArray = (VKUsersArray) response.parsedModel;
                        VKApiUserFull userFull = usersArray.get(0);
                        doAfterLogin.addObject("userFull", userFull);
                        doAfterLogin.run();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                    super.attemptFailed(request, attemptNumber, totalAttempts);

                }

                @Override
                public void onError(VKError error) {
                    super.onError(error);
                    new AlertDialog.Builder(VKUIHelper.getTopActivity())
                            .setMessage(error.toString())
                            .show();
                }
            });
        }

        @Override
        public void onAcceptUserToken(VKAccessToken token) {
            Log.d("onAcceptUserToken", "" + token);
            //            Intent i = new Intent(TeleFMApplication.this, MainActivity.class);
//            startActivity(i);
            getVKUserData();
        }

        @Override
        public void onRenewAccessToken(VKAccessToken token) {
            super.onRenewAccessToken(token);
        }
    }

    static class OKCallback implements OkTokenRequestListener {

        @Nullable
        DoAfterLogin doAfterLogin;
        CallbackApi2<SignedUser> callback;

        public void setDoAfterLogin(@Nullable DoAfterLogin doAfterLogin) {
            this.doAfterLogin = doAfterLogin;
        }

        public void setCallback(CallbackApi2<SignedUser> callback) {
            this.callback = callback;
        }

        @Override
        public void onSuccess(String s) {
            new GetCurrentOKUser().execute(new Void[0]);
        }

        @Override
        public void onError() {

        }

        @Override
        public void onCancel() {

        }

        class GetCurrentOKUser extends AsyncTask<Void, Void, String> {

            @Override
            protected String doInBackground(final Void... params) {
                try {
                    return mOdnoklassniki.request("users.getCurrentUser", null, "get");
                } catch (Exception exc) {
                    Log.e("Odnoklassniki", "Failed to get current user info", exc);
                }
                return null;
            }

            @Override
            protected void onPostExecute(final String result) {
                if (result != null) {
                    doAfterLogin.addObject("OKUser", result);
                    doAfterLogin.run();
//                Toast.makeText(mContext, "Get current user result: " + result, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    abstract static class DoAfterLogin implements Runnable {
        HashMap<String, Object> map = new HashMap<>();

        public DoAfterLogin addObject(String name, Object object) {
            map.put(name, object);
            return this;
        }

        @Override
        abstract public void run();
    }
}
