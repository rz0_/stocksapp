package com.apps.rz0.actionapp.choose_dialog;

import com.google.gson.annotations.SerializedName;

/**
 * Класс для парсинга полученного с сервера города
 */
public class ChoosedCity {
    private Boolean success;

    @SerializedName("city_id")
    private long cityId;

    private String city;

    public long getCityId() {
        return cityId;
    }

    public String getCity() {
        return city;
    }
}
