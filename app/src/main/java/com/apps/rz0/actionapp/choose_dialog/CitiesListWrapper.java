package com.apps.rz0.actionapp.choose_dialog;

import java.util.ArrayList;

/**
 * Класс-обертка для получения списка городов с сервера
 */
public class CitiesListWrapper {
    private ArrayList<CityItem> cities = new ArrayList<CityItem>();

    public ArrayList<CityItem> getCitiesList() {
        return cities;
    }
}
